namespace Proactive.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _migration_Sellertable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Infringement", "SellerId", c => c.Int(nullable: true));
            CreateIndex("dbo.Infringement", "SellerId");
            AddForeignKey("dbo.Infringement", "SellerId", "dbo.Sellers", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Infringement", "SellerId", "dbo.Sellers");
            DropIndex("dbo.Infringement", new[] { "SellerId" });
            DropColumn("dbo.Infringement", "SellerId");
        }
    }
}
