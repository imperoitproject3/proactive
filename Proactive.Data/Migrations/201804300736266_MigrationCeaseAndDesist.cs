namespace Proactive.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class MigrationCeaseAndDesist : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CeaseAndDesist",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    FileName = c.String(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    UpdatedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.InfringementCeaseAndDesist",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CeaseAndDesistId = c.Int(nullable: false),
                    InfringementId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CeaseAndDesist", t => t.CeaseAndDesistId, cascadeDelete: true)
                .ForeignKey("dbo.Infringement", t => t.InfringementId, cascadeDelete: true)
                .Index(t => t.CeaseAndDesistId)
                .Index(t => t.InfringementId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.InfringementCeaseAndDesist", "InfringementId", "dbo.Infringement");
            DropForeignKey("dbo.InfringementCeaseAndDesist", "CeaseAndDesistId", "dbo.CeaseAndDesist");
            DropIndex("dbo.InfringementCeaseAndDesist", new[] { "InfringementId" });
            DropIndex("dbo.InfringementCeaseAndDesist", new[] { "CeaseAndDesistId" });
            DropTable("dbo.InfringementCeaseAndDesist");
            DropTable("dbo.CeaseAndDesist");
        }
    }
}
