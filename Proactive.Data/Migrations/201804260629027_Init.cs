namespace Proactive.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientInsightsAndTrends",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    InsightsAndTrendsId = c.Int(nullable: false),
                    ClientId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.InsightsAndTrends", t => t.InsightsAndTrendsId, cascadeDelete: true)
                .Index(t => t.InsightsAndTrendsId)
                .Index(t => t.ClientId);

            CreateTable(
                "dbo.Users",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    FirstName = c.String(nullable: false),
                    UserRole = c.Int(nullable: false),
                    CountryId = c.Int(nullable: false),
                    Source = c.String(),
                    Remark = c.String(unicode: false),
                    IsActive = c.Boolean(nullable: false),
                    Email = c.String(maxLength: 256),
                    EmailConfirmed = c.Boolean(nullable: false),
                    PasswordHash = c.String(),
                    SecurityStamp = c.String(),
                    PhoneNumber = c.String(),
                    PhoneNumberConfirmed = c.Boolean(nullable: false),
                    TwoFactorEnabled = c.Boolean(nullable: false),
                    LockoutEndDateUtc = c.DateTime(),
                    LockoutEnabled = c.Boolean(nullable: false),
                    AccessFailedCount = c.Int(nullable: false),
                    UserName = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Country", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");

            CreateTable(
                "dbo.UserClaim",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.String(nullable: false, maxLength: 128),
                    ClaimType = c.String(),
                    ClaimValue = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.Country",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 50),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "Unique_countryName");

            CreateTable(
                "dbo.Infringement",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Title = c.String(nullable: false),
                    Price = c.Double(nullable: false),
                    Url = c.String(),
                    ItemNumber = c.String(),
                    Quantity = c.Int(nullable: false),
                    SellerLocation = c.String(),
                    ItemLocation = c.String(),
                    Remark = c.String(unicode: false),
                    UserId = c.String(nullable: false, maxLength: 128),
                    ClientId = c.String(nullable: false, maxLength: 128),
                    PlatformId = c.Int(nullable: false),
                    InfringementTypeId = c.Int(nullable: false),
                    ReviewStatus = c.Int(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    UpdatedDate = c.DateTime(),
                    Users_Id = c.String(maxLength: 128),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ClientId, cascadeDelete: false)
                .ForeignKey("dbo.InfringementType", t => t.InfringementTypeId, cascadeDelete: false)
                .ForeignKey("dbo.Platform", t => t.PlatformId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.Users_Id)
                .Index(t => t.UserId)
                .Index(t => t.ClientId)
                .Index(t => t.PlatformId)
                .Index(t => t.InfringementTypeId)
                .Index(t => t.Users_Id);

            CreateTable(
                "dbo.InfringementImage",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    InfringementId = c.Int(nullable: false),
                    ImageName = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Infringement", t => t.InfringementId, cascadeDelete: true)
                .Index(t => t.InfringementId);

            CreateTable(
                "dbo.InfringementType",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 50),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "Unique_infringementType");

            CreateTable(
                "dbo.Platform",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false),
                    Url = c.String(nullable: false),
                    CountryId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Country", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);

            CreateTable(
                "dbo.UserLogin",
                c => new
                {
                    LoginProvider = c.String(nullable: false, maxLength: 128),
                    ProviderKey = c.String(nullable: false, maxLength: 128),
                    UserId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.UserRole",
                c => new
                {
                    UserId = c.String(nullable: false, maxLength: 128),
                    RoleId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.RoleMaster", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.InsightsAndTrends",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Description = c.String(unicode: false),
                    CreatedDate = c.DateTime(nullable: false),
                    UpdatedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.RoleMaster",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    Name = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

        }

        public override void Down()
        {
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.RoleMaster");
            DropForeignKey("dbo.ClientInsightsAndTrends", "InsightsAndTrendsId", "dbo.InsightsAndTrends");
            DropForeignKey("dbo.ClientInsightsAndTrends", "ClientId", "dbo.Users");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.Users");
            DropForeignKey("dbo.Infringement", "Users_Id", "dbo.Users");
            DropForeignKey("dbo.Infringement", "UserId", "dbo.Users");
            DropForeignKey("dbo.Infringement", "PlatformId", "dbo.Platform");
            DropForeignKey("dbo.Platform", "CountryId", "dbo.Country");
            DropForeignKey("dbo.Infringement", "InfringementTypeId", "dbo.InfringementType");
            DropForeignKey("dbo.InfringementImage", "InfringementId", "dbo.Infringement");
            DropForeignKey("dbo.Infringement", "ClientId", "dbo.Users");
            DropForeignKey("dbo.Users", "CountryId", "dbo.Country");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.Users");
            DropIndex("dbo.RoleMaster", "RoleNameIndex");
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.Platform", new[] { "CountryId" });
            DropIndex("dbo.InfringementType", "Unique_infringementType");
            DropIndex("dbo.InfringementImage", new[] { "InfringementId" });
            DropIndex("dbo.Infringement", new[] { "Users_Id" });
            DropIndex("dbo.Infringement", new[] { "InfringementTypeId" });
            DropIndex("dbo.Infringement", new[] { "PlatformId" });
            DropIndex("dbo.Infringement", new[] { "ClientId" });
            DropIndex("dbo.Infringement", new[] { "UserId" });
            DropIndex("dbo.Country", "Unique_countryName");
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.Users", "UserNameIndex");
            DropIndex("dbo.Users", new[] { "CountryId" });
            DropIndex("dbo.ClientInsightsAndTrends", new[] { "ClientId" });
            DropIndex("dbo.ClientInsightsAndTrends", new[] { "InsightsAndTrendsId" });
            DropTable("dbo.RoleMaster");
            DropTable("dbo.InsightsAndTrends");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserLogin");
            DropTable("dbo.Platform");
            DropTable("dbo.InfringementType");
            DropTable("dbo.InfringementImage");
            DropTable("dbo.Infringement");
            DropTable("dbo.Country");
            DropTable("dbo.UserClaim");
            DropTable("dbo.Users");
            DropTable("dbo.ClientInsightsAndTrends");
        }
    }
}
