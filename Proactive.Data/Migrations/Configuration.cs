﻿using Microsoft.AspNet.Identity.EntityFramework;
using Proactive.Core.Enumerations;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using Proactive.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            var roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));
            var userManager = new ApplicationUserManager(new ApplicationStore(context));
            #region Insert Roles if not exist

            string roleName = Role.Admin.ToString();
            var role = roleManager.FindByNameAsync(roleName).Result;
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.CreateAsync(role).Result;
            }

            roleName = Role.Client.ToString();
            role = roleManager.FindByNameAsync(roleName).Result;
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.CreateAsync(role).Result;
            }

            roleName = Role.User.ToString();
            role = roleManager.FindByNameAsync(roleName).Result;
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.CreateAsync(role).Result;
            }
            #endregion

            #region Add Admin User
            const string emailId = "admin@Proactive.com";
            const string userName = "Admin";
            const string password = "Proactive@123";
            var user = userManager.FindByNameAsync(userName).Result;
            if (user == null)
            {
                user = new Users() { UserName = userName, Email = emailId, FirstName = userName, CountryId = 1, UserRole = Role.Admin };

                var result = userManager.CreateAsync(user, password).Result;
                result = userManager.SetLockoutEnabledAsync(user.Id, false).Result;
            }
            #endregion

            #region Add Admin Role to Admin User
            var rolesForUser = userManager.GetRolesAsync(user.Id).Result;
            if (!rolesForUser.Contains(Role.Admin.ToString()))
            {
                var result = userManager.AddToRoleAsync(user.Id, Role.Admin.ToString()).Result;
            }
            #endregion
        }
    }
}
