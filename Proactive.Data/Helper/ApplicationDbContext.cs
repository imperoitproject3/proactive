﻿using Microsoft.AspNet.Identity.EntityFramework;
using Proactive.Data.Entities;
using Proactive.Data.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Helper
{
    public class ApplicationDbContext : IdentityDbContext<Users>
    {
        public ApplicationDbContext() : base("connectionString")
        {
            //Configuration.ProxyCreationEnabled = false;
            //Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Users>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("RoleMaster");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
        }

        #region User entities
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<ClientInsightsAndTrends> ClientInsightsAndTrends { get; set; }
        public virtual DbSet<Infringement> Infringement { get; set; }
        public virtual DbSet<InfringementImage> InfringementImage { get; set; }
        public virtual DbSet<InfringementType> InfringementType { get; set; }
        public virtual DbSet<InsightsAndTrends> InsightsAndTrends { get; set; }
        public virtual DbSet<Platform> Platform { get; set; }
        public virtual DbSet<CeaseAndDesist> CeaseAndDesist { get; set; }
        public virtual DbSet<InfringementCeaseAndDesist> InfringementCeaseAndDesist { get; set; }
        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<ResourceContents> Contents { get; set; }
        public virtual DbSet<Sellers> Sellers { get; set; }
        public virtual DbSet<SellerAccounts> SellerAccounts { get; set; }
        public virtual DbSet<SellerHighriskAccount> SellerHighriskAccounts { get; set; }
        #endregion
    }
}
