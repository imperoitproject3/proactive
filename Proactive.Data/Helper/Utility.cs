﻿using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Helper
{
    public static class Utility
    {
        public static DateTime GetSystemDateTimeUTC()
        {
            return DateTime.UtcNow;
        }

        public static string GenerateEmailBody(EmailTemplate templateFileName, Dictionary<string, string> dicPlaceholders)
        {
            dicPlaceholders.Add("{{projectName}}", GlobalConfig.ProjectName);
            string templatePath = Path.Combine(GlobalConfig.EmailTemplatePath, templateFileName.ToString() + ".html");
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(templatePath))
            {
                body = reader.ReadToEnd();
            }
            if (body.Length > 0)
            {
                foreach (var item in dicPlaceholders)
                {
                    body = body.Replace(item.Key, item.Value);
                }
            }
            return body;
        }

        public static bool IEquals(this string value, string compare)
        {
            if (value == null && compare == null)
                return true;
            if ((value == null && compare != null) || (value != null && compare == null))
                return false;
            return value.Equals(compare, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
