﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class ClientInsightsAndTrends : BaseIdEntity
    {
        [Required]
        public int InsightsAndTrendsId { get; set; }
        [ForeignKey("InsightsAndTrendsId")]
        public virtual InsightsAndTrends InsightsAndTrends { get; set; }

        [Required]
        public string ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual Users Client { get; set; }
    }
}
