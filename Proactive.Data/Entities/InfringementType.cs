﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class InfringementType:BaseIdEntity
    {
        [Required]
        [StringLength(50)]
        [Index("Unique_infringementType", IsUnique = true)]
        public string Name { get; set; }

        public virtual ICollection<Infringement> Infringement { get; set; }

    }
}
