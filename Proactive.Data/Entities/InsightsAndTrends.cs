﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class InsightsAndTrends : BaseEntity
    {
        [Required]
        public string Title { get; set; }

        [Column(TypeName = "Varchar(max)")]
        public string Description { get; set; }

        public bool IsViewOnWeb { get; set; } = false;

        public string ImageName { get; set; }

        public virtual List<ClientInsightsAndTrends> ClientInsightsAndTrends { get; set; }
    }
}
