﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class CeaseAndDesist : BaseEntity
    {
        [Required]
        public string FileName { get; set; }

        public virtual List<InfringementCeaseAndDesist> InfringementCeaseAndDesist { get; set; }
    }
}

