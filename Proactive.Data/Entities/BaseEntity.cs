﻿using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class BaseEntity : BaseIdEntity
    {
        public DateTime CreatedDate { get; set; } = Utility.GetSystemDateTimeUTC();
        public DateTime? UpdatedDate { get; set; }
    }
}
