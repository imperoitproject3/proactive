﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class ResourceContents : BaseIdEntity
    {
        [Required]
        public string Title { get; set; }
        
        public string ContentImage { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Discription { get; set; }

        public Contents ContentType { get; set; }

        public Resources Resourcetype { get; set; }
    }
}
