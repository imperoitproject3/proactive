﻿using Microsoft.AspNet.Identity.EntityFramework;
using Proactive.Core.Enumerations;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class Users : IdentityUser
    {
        [Required]
        public string FirstName { get; set; }

        [EnumDataType(typeof(Role))]
        public Role UserRole { get; set; }

        public int CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        public string Source { get; set; }

        [Column(TypeName = "Varchar(max)")]
        public string Remark { get; set; }

        public bool IsActive { get; set; } = true;

        public DateTime CreatedDate { get; set; } = Utility.GetSystemDateTimeUTC();

        //public virtual ICollection<Infringement> Infringement { get; set; }
    }
}
