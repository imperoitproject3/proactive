﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class InfringementCeaseAndDesist : BaseIdEntity
    {
        [Required]
        public int CeaseAndDesistId { get; set; }
        [ForeignKey("CeaseAndDesistId")]
        public virtual CeaseAndDesist CeaseAndDesist { get; set; }

        [Required]
        public int InfringementId { get; set; }
        [ForeignKey("InfringementId")]
        public virtual Infringement Infringement { get; set; }

        public string Source { get; set; }

    }
}
