﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class Infringement : BaseEntity
    {
        [Required]
        public string Title { get; set; }

        public double Price { get; set; }

        public string Url { get; set; }

        public string ItemNumber { get; set; }

        public int Quantity { get; set; }

        public string SellerLocation { get; set; }

        public string ItemLocation { get; set; }

        public bool IsSendForRview { get; set; }

        public string RetractReason { get; set; }

        [Column(TypeName = "Varchar(max)")]
        public string Remark { get; set; }

        [Required]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual Users User { get; set; }

        [Required]
        public string ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual Users Client { get; set; }

        [Required]
        public int PlatformId { get; set; }
        [ForeignKey("PlatformId")]
        public virtual Platform Platform { get; set; }

        public int? SellerId { get; set; }
        [ForeignKey("SellerId")]
        public virtual Sellers Seller { get; set; }

        [Required]
        public int InfringementTypeId { get; set; }
        [ForeignKey("InfringementTypeId")]
        public virtual InfringementType InfringementType { get; set; }

        public bool IsDelete { get; set; }

        [Required]
        public ReviewStatus ReviewStatus { get; set; }

        public virtual ICollection<InfringementImage> InfringementImage { get; set; }
        public virtual ICollection<InfringementCeaseAndDesist> InfringementCeaseAndDesist { get; set; }

    }
}
