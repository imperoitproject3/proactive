﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class InfringementImage : BaseIdEntity
    {
        public int InfringementId { get; set; }
        [ForeignKey("InfringementId")]
        public virtual Infringement Infringement { get; set; }

        [Required]
        public string ImageName { get; set; }
    }
}
