﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class Blog : BaseEntity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Author { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Description { get; set; }

        public string ImageName { get; set; }

        public PageContent PageContentType { get; set; }
    }
}
