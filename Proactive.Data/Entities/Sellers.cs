﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Entities
{
    public class Sellers : BaseEntity
    {
        public string Name { get; set; }

        public string ContactDetail { get; set; }

        public bool IsLinkedAccount { get; set; }

        public bool IsHighriskAccount { get; set; }

        public virtual ICollection<SellerAccounts> SellerAccounts { get; set; }
        public virtual ICollection<SellerHighriskAccount> SellerHighriskAccount { get; set; }
        public virtual ICollection<Infringement> Infringement { get; set; }
    }

    public class SellerAccounts : BaseIdEntity
    {
        public string LinkedAccountName { get; set; }               

        public int SellerId { get; set; }
        [ForeignKey("SellerId")]
        public virtual Sellers Seller { get; set; }
    }

    public class SellerHighriskAccount : BaseIdEntity
    {
        public string HighriskAccountName { get; set; }

        public int SellerId { get; set; }
        [ForeignKey("SellerId")]
        public virtual Sellers Seller { get; set; }
    }
}
