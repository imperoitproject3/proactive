﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{
    public class CountryRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        private string loginUserId { get; set; }
        public CountryRepository()
        {
            _ctx = new ApplicationDbContext();
        }
        public CountryRepository(string loginUserId)
        {
            _ctx = new ApplicationDbContext();
            this.loginUserId = loginUserId;
        }
        public async Task<ResponseModel<object>> AddorUpdate(LookupBaseModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = string.Format("{0} country {1}", model.name, "Record update!")
            };
            var entity = await _ctx.Country.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (entity == null)
            {
                entity = new Country();
                entity.Name = model.name;
                _ctx.Country.Add(entity);
                mResult.Message = string.Format("{0} country {1}", model.name, "record Added");
            }
            else
            {
                entity.Name = model.name;
            }
            await _ctx.SaveChangesAsync();
            return mResult;
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.Country.FirstOrDefaultAsync(x => x.Id == Id);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("{0} country {1}", entity.Name,"deleted");

                _ctx.Country.Remove(entity);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public async Task<LookupBaseModel> GetCountry(int Id)
        {
            return await (from x in _ctx.Country
                          where x.Id == Id
                          select new LookupBaseModel
                          {
                              Id = x.Id,
                              name = x.Name,
                          }).FirstOrDefaultAsync();
        }
        public IQueryable<CountryViewModel> GetCountries()
        {
            return (from x in _ctx.Country
                    select new CountryViewModel
                    {
                        Id = x.Id,
                        name = x.Name,
                    }).OrderBy(c => c.name);
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
