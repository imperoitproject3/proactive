﻿using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{
    public class BlogRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        public BlogRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<BlogsModel> GetBlogs(PageContent PageContent)
        {
            return (from x in _ctx.Blogs
                    where x.PageContentType == PageContent
                    select new BlogsModel
                    {
                        Id = x.Id,
                        Description = x.Description,
                        Author = x.Author,
                        Title = x.Title,
                        ContentType = x.PageContentType.ToString(),
                        Published = x.CreatedDate
                    }).OrderByDescending(c => c.Id);
        }

        public IQueryable<BlogsModel> GetBlogs()
        {
            return (from x in _ctx.Blogs
                    select new BlogsModel
                    {
                        Id = x.Id,
                        Description = x.Description,
                        Author = x.Author,
                        Title = x.Title,
                        ContentType = x.PageContentType.ToString(),
                        Published = x.CreatedDate
                    }).OrderByDescending(c => c.Id);
        }

        //public List<BlogsModel> GetBlogsList()
        //{
        //    List<BlogsModel> responce = new List<BlogsModel>();
        //    var objBlogs = (from x in _ctx.Blogs
        //                    select new
        //                    {
        //                        Id = x.Id,
        //                        Description = x.Description,
        //                        Author = x.Author,
        //                        Title = x.Title,
        //                        PublishDate = x.CreatedDate
        //                    }).OrderByDescending(c => c.PublishDate);
        //    if (objBlogs != null)
        //    {
        //        responce = objBlogs.Select(x => new BlogsModel()
        //        {
        //            Id = x.Id,
        //            Published = "Published on " + x.PublishDate.ToLongDateString() + " by " + x.Author,
        //            Author = x.Author,
        //            Description = x.Description,
        //            Title = x.Title
        //        }).ToList();
        //    }
        //    return responce;
        //}

        public async Task<ResponseModel<object>> AddorUpdate(BlogsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = string.Format("Blog update successfully!")
            };
            var entity = await _ctx.Blogs.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (entity == null)
            {
                entity = new Blog();
                entity.Description = model.Description;
                entity.Title = model.Title;
                entity.Author = model.Author;
                entity.ImageName = model.imageName;
                entity.PageContentType = model.PageContentType;
                _ctx.Blogs.Add(entity);
                mResult.Message = string.Format("Blog added successfully!");

            }
            else
            {
                entity.Description = model.Description;
                entity.Author = model.Author;
                entity.Title = model.Title;
                entity.ImageName = model.imageName;
                entity.PageContentType = model.PageContentType;
            }
            await _ctx.SaveChangesAsync();
            return mResult;
        }

        public async Task<BlogsModel> GetBlogDetails(int Id)
        {
            BlogsModel model = new BlogsModel();
            var objData = await (from x in _ctx.Blogs
                                 where x.Id == Id
                                 select new
                                 {
                                     Id = x.Id,
                                     Description = x.Description,
                                     Title = x.Title,
                                     Author = x.Author,
                                     x.PageContentType,
                                     x.ImageName
                                 }).FirstOrDefaultAsync();
            if (objData != null)
            {
                model.Id = objData.Id;
                model.Author = objData.Author;
                model.Description = objData.Description;
                model.Title = objData.Title;
                model.PageContentType = objData.PageContentType;
                model.imageName = objData.ImageName;
            }
            return model;
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.Blogs.FirstOrDefaultAsync(x => x.Id == Id);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("record deleted");
                string FilePath = GlobalConfig.BlogImagePath + entity.ImageName;
                if (System.IO.File.Exists(FilePath))
                {
                    System.IO.File.Delete(FilePath);
                }
                _ctx.Blogs.Remove(entity);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public BlogsModel GetPagelatestContent(PageContent PageContent)
        {
            var objData = (from x in _ctx.Blogs
                           where x.PageContentType == PageContent
                           orderby x.CreatedDate descending
                           select new BlogsModel
                           {
                               Id = x.Id,
                               Description = x.Description,
                               Author = x.Author,
                               Title = x.Title,
                               ContentType = x.PageContentType.ToString(),
                               Published = x.CreatedDate,
                               imageName = x.ImageName
                           }).FirstOrDefault();
            return objData;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
