﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using Proactive.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace Proactive.Data.Repository
{
    public class UserRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        private readonly ApplicationUserManager _userManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IOwinContext _owin;

        public UserRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<Users>(_ctx));
        }

        public UserRepository(IOwinContext owin)
        {
            _owin = owin;
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<Users>(_ctx));
            _userManager.EmailService = new EmailService();
            _authenticationManager = owin.Authentication;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByEmailAsync(model.EmailId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid username.";
            }
            else if (!user.IsActive)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Your account blocked by admin please contact to Administrator";
            }
            else
            {
                var validRole = await _userManager.IsInRoleAsync(user.Id, Role.Admin.ToString());
                if (!validRole)
                    validRole = await _userManager.IsInRoleAsync(user.Id, Role.Client.ToString());

                var validPassword = await _userManager.CheckPasswordAsync(user, model.Password);

                if (validRole)
                {
                    if (validPassword)
                    {
                        await SighnInAsync(user, false, model.RememberMe);
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = string.Format("User {0} Logged in successfully!", user.UserName);
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Invalid access.";
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            var userName = _owin.Authentication.User.Identity.Name;
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid username or password.";
            }
            else
            {
                IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Password changed successfully";
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Invalid password.";
                }
            }
            return mResult;
        }

        private async Task SighnInAsync(Users user, bool isPersistent, bool rememberBrowser)
        {
            var userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("FullName", user.FirstName));
            userIdentity.AddClaim(new Claim("UserRole", user.UserRole.ToString()));
            _authenticationManager.SignIn(
                new AuthenticationProperties { IsPersistent = isPersistent },
                userIdentity);
        }

        public void WebLogout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
               DefaultAuthenticationTypes.TwoFactorCookie);
        }

        public async Task<ResponseModel<bool>> ActiveToggle(string userId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user != null)
            {
                user.IsActive = !user.IsActive;
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                if (user.IsActive)
                {
                    mResult.Message = "Activated successfully.";
                }
                else
                {
                    mResult.Message = "Deactivated successfully.";
                }
                mResult.Result = user.IsActive;
            }
            else
            {
                mResult.Message = "User not found.";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> Delete(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            var user = _ctx.Users.FirstOrDefault(p => p.Id == UserId);
            if (user != null)
            {
                var infregment = await _ctx.Infringement.Where(x => x.UserId == user.Id || x.ClientId == user.Id).ToListAsync();

                if (infregment.Count() > 0)
                {
                    foreach (var itm in infregment)
                    {
                        var infregmentimg = await _ctx.InfringementImage.Where(x => x.InfringementId == itm.Id).ToListAsync();
                        foreach(var i in infregmentimg)
                        {
                            string FilePath = GlobalConfig.InfringementPath + i.ImageName;
                            if (System.IO.File.Exists(FilePath))
                            {
                                System.IO.File.Delete(FilePath);
                            }
                        }  
                        _ctx.InfringementImage.RemoveRange(infregmentimg);
                    }
                    _ctx.Infringement.RemoveRange(infregment);
                }

                var CLientInsiderends = await _ctx.ClientInsightsAndTrends.Where(x => x.ClientId == user.Id).ToListAsync();
                if (CLientInsiderends.Count > 0)
                {
                    _ctx.ClientInsightsAndTrends.RemoveRange(CLientInsiderends);
                }
                _ctx.Users.Remove(user);
                try
                {
                    await _ctx.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                mResult.Status = ResponseStatus.Success;
                mResult.Message = "User deleted successfully.";
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AddorUpdate(UserAddUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success
            };

            if (string.IsNullOrEmpty(model.userId))
            {
                model.Password = Membership.GeneratePassword(10, 3);
                mResult = await Signup(model);
                try
                {
                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", model.Name);
                    dicPlaceholders.Add("{{Username}}", model.EmailId);
                    dicPlaceholders.Add("{{Password}}", model.Password);
                    dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                    dicPlaceholders.Add("{{link}}", GlobalConfig.baseUrl + "/Admin");
                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format("Welcome to: {0}", GlobalConfig.ProjectName);
                    message.Body = Utility.GenerateEmailBody(EmailTemplate.RegisterClient, dicPlaceholders);
                    message.Destination = model.EmailId;
                    await new EmailService().SendAsyncClient(message);
                    mResult.Status = ResponseStatus.Success;
                }
                catch (Exception e)
                {
                    mResult.Message = e.Message;
                    mResult.Status = ResponseStatus.Failed;
                }
            }
            else
            {
                var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == model.userId);
                if (user != null)
                {
                    user.Email = model.EmailId;
                    user.CountryId = model.CountryId;
                    user.FirstName = model.Name;
                    user.Remark = model.Remark;
                    user.Source = model.Source;
                    await _ctx.SaveChangesAsync();
                    mResult.Message = string.Format("{0} user {1}", model.Name, "record update!");
                }
                else
                {
                    mResult.Status = ResponseStatus.NotFound;
                    mResult.Message = "Resource can't be found";
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> Signup(UserAddUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Failed
            };
            if (!string.IsNullOrEmpty(model.EmailId))
            {
                var user = await _userManager.FindByEmailAsync(model.EmailId);
                if (user == null)
                {
                    user = new Users()
                    {
                        UserName = model.EmailId,
                        Email = model.EmailId,
                        FirstName = model.Name,
                        UserRole = model.Role,
                        CountryId = model.CountryId,
                        Remark = model.Remark,
                        Source = model.Source
                    };

                    IdentityResult result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        result = await _userManager.AddToRoleAsync(user.Id, model.Role.ToString());
                        if (result.Succeeded)
                        {
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = "User added successfully";
                        }
                        else
                        {
                            mResult.Status = ResponseStatus.Failed;
                            mResult.Message = string.Join("; ", result.Errors);
                        }
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = string.Join("; ", result.Errors);
                    }
                }
                else
                {
                    mResult.Message = "This email address already used";
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Please enter email address";
            }
            return mResult;
        }

        public async Task<UserAddUpdateModel> GetUser(string userId)
        {
            return await (from x in _ctx.Users
                          where x.Id == userId
                          select new UserAddUpdateModel
                          {
                              userId = x.Id,
                              CountryId = x.CountryId,
                              EmailId = x.Email,
                              Name = x.FirstName,
                              Remark = x.Remark,
                              Source = x.Source,
                          }).FirstOrDefaultAsync();
        }

        public IQueryable<UserViewModel> GetUsers()
        {
            return (from x in _ctx.Users
                    where x.UserRole != Role.Admin
                    select new UserViewModel
                    {
                        UserId = x.Id,
                        Name = x.FirstName,
                        Country = x.Country.Name,
                        EmailId = x.Email,
                        Renark = x.Remark,
                        Source = x.Source,
                        IsActive = x.IsActive,
                        CreateDate = x.CreatedDate
                    }).OrderByDescending(x => x.CreateDate);
        }

        public async Task<ResponseModel<object>> ForgotPassword(ForgotPasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Failed
            };

            var user = await _userManager.FindByEmailAsync(model.email);
            if (user != null)
            {
                var Password = Membership.GeneratePassword(10, 3);
                user.PasswordHash = _userManager.PasswordHasher.HashPassword(Password);
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    try
                    {
                        Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                        dicPlaceholders.Add("{{name}}", user.FirstName);
                        dicPlaceholders.Add("{{Username}}", user.UserName);
                        dicPlaceholders.Add("{{Password}}", Password);
                        dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                        dicPlaceholders.Add("{{link}}", GlobalConfig.baseUrl + "/Admin");

                        IdentityMessage message = new IdentityMessage();
                        message.Subject = string.Format("Reset Password : {0}", GlobalConfig.ProjectName);
                        message.Body = Utility.GenerateEmailBody(EmailTemplate.ForgotPassword, dicPlaceholders);
                        message.Destination = user.Email;
                        await new EmailService().SendAsync(message);
                        mResult.Message = "Email Send successfully";
                        mResult.Status = ResponseStatus.Success;
                    }
                    catch (Exception e)
                    {
                        mResult.Message = e.Message;
                        mResult.Status = ResponseStatus.Failed;
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = string.Join("; ", result.Errors);
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "You are not register with this email";
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
