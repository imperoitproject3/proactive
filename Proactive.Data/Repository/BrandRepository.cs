﻿using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{

    public class BrandRepository
    {
        private readonly ApplicationDbContext _ctx;
        public BrandRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<ResourceContentModel> GetContents(int Id)
        {
            ResourceContentModel mResult = new ResourceContentModel();
            var result = await (from whiteppr in _ctx.Contents
                                where whiteppr.Id == Id
                                select new ResourceContentModel
                                {
                                    Id = whiteppr.Id,
                                    Title = whiteppr.Title,
                                    imageName = whiteppr.ContentImage,
                                    Description = whiteppr.Discription,
                                    ContentType = whiteppr.ContentType,
                                    ResourceType = whiteppr.Resourcetype,
                                }).FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.Contents.FirstOrDefaultAsync(x => x.Id == Id);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("{0} Platform {1}", entity.Title, "record deleted");

                _ctx.Contents.Remove(entity);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public ResourceContentModel GetContentForWeb(Contents contentType, Resources Resourcetype)
        {
            ResourceContentModel mResult = new ResourceContentModel();
            var result = (from whiteppr in _ctx.Contents
                          where whiteppr.ContentType == contentType
                          && whiteppr.Resourcetype == Resourcetype
                          select new ResourceContentModel
                          {
                              Id = whiteppr.Id,
                              Title = whiteppr.Title,
                              imageName = whiteppr.ContentImage,
                              Description = whiteppr.Discription,
                              ContentType = whiteppr.ContentType,
                          }).FirstOrDefault();
            if (result != null)
                result.StrContentType = getEnumString(result.ContentType);
            return result;
        }

        public ResourceContentModel GetContentForWeb(int Id)
        {
            ResourceContentModel mResult = new ResourceContentModel();
            var result = (from whiteppr in _ctx.Contents
                          where whiteppr.Id == Id
                          select new ResourceContentModel
                          {
                              Id = whiteppr.Id,
                              Title = whiteppr.Title,
                              imageName = whiteppr.ContentImage,
                              Description = whiteppr.Discription,
                              ContentType = whiteppr.ContentType,
                          }).FirstOrDefault();
            if (result != null)
                result.StrContentType = getEnumString(result.ContentType);
            return result;
        }

        public List<ResourceContentModel> GetContentForWeb(Resources Resourcetype)
        {
            ResourceContentModel mResult = new ResourceContentModel();
            var result = (from whiteppr in _ctx.Contents
                          where whiteppr.Resourcetype == Resourcetype
                          select new ResourceContentModel
                          {
                              Id = whiteppr.Id,
                              Title = whiteppr.Title,
                              imageName = whiteppr.ContentImage,
                              Description = whiteppr.Discription,
                              ContentType = whiteppr.ContentType,
                              ResourceType = whiteppr.Resourcetype
                          }).ToList();
            result.ForEach(x => x.StrContentType = getEnumString(x.ContentType));
            return result;
        }

        public async Task<ResponseModel<object>> AddUpdateBrandProtection(ResourceContentModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = model.ContentType.ToString() + " update successfully!"
            };
            var entity = await _ctx.Contents.FirstOrDefaultAsync(x => x.ContentType == model.ContentType);
            if (entity == null)
            {
                entity = new ResourceContents();
                entity.Discription = model.Description;
                entity.Title = model.Title;
                entity.ContentImage = model.imageName;
                entity.ContentType = model.ContentType;
                entity.Resourcetype = model.ResourceType;
                _ctx.Contents.Add(entity);
                mResult.Message = model.ContentType.ToString() + " added successfully!";

            }
            else
            {
                entity.Discription = model.Description;
                entity.Title = model.Title;
                entity.ContentImage = model.imageName;
                entity.ContentType = model.ContentType;
                entity.Resourcetype = model.ResourceType;
            }
            await _ctx.SaveChangesAsync();
            return mResult;
        }

        public List<ResourceContentModel> GetContentList(Contents contentType, Resources Resourcetype)
        {
            ResourceContentModel mResult = new ResourceContentModel();
            var result = (from whiteppr in _ctx.Contents
                          where whiteppr.Resourcetype == Resourcetype
                          && whiteppr.ContentType == contentType
                          select new ResourceContentModel
                          {
                              Id = whiteppr.Id,
                              Title = whiteppr.Title,
                              imageName = whiteppr.ContentImage,
                              Description = whiteppr.Discription,
                              ContentType = whiteppr.ContentType,
                              ResourceType = whiteppr.Resourcetype
                          }).ToList();
            if (result.Count > 0)
            {
                result.ForEach(x =>
                {
                    x.StrContentType = getEnumString(x.ContentType);
                });
            }
            return result;
        }

        public List<ResourceContentModel> GetContentServices(Resources Resourcetype)
        {
            var result = (from cn in _ctx.Contents
                          where cn.Resourcetype==Resourcetype
                          group cn by cn.ContentType
                         into g
                          select new ResourceContentModel
                          {
                              ContentType = g.FirstOrDefault().ContentType,
                              Description = g.FirstOrDefault().Discription,
                              Id = g.FirstOrDefault().Id,
                              imageName = g.FirstOrDefault().ContentImage,
                              ResourceType = g.FirstOrDefault().Resourcetype,
                              Title = g.FirstOrDefault().Title,
                          }).ToList();

            result.ForEach(x =>
            {
                x.StrContentType = getEnumString(x.ContentType);
            });

            return result;
        }

        public List<ResourceContentModel> GetContentForServiceList(Contents contentType, Resources Resourcetype, int Id)
        {
            ResourceContentModel mResult = new ResourceContentModel();

            var result = (from whiteppr in _ctx.Contents
                          where whiteppr.Resourcetype == Resourcetype
                          && whiteppr.ContentType == contentType
                          && whiteppr.Id != Id
                          select new ResourceContentModel
                          {
                              Id = whiteppr.Id,
                              Title = whiteppr.Title,
                              imageName = whiteppr.ContentImage,
                              Description = whiteppr.Discription,
                              ContentType = whiteppr.ContentType,
                              ResourceType = whiteppr.Resourcetype
                          }).ToList();
            if (result.Count > 0)
            {
                result.ForEach(x =>
                {
                    x.StrContentType = getEnumString(x.ContentType);
                });
            }
            return result;
        }

        public IQueryable<ResourceContentModel> GetContentList()
        {
            ResourceContentModel mResult = new ResourceContentModel();
            var result = (from whiteppr in _ctx.Contents
                          where whiteppr.Resourcetype == Resources.Anticounterfeiting
                          || whiteppr.Resourcetype == Resources.Antipiracy
                          || whiteppr.Resourcetype == Resources.BrandProtection
                          orderby whiteppr.Id descending
                          select new ResourceContentModel
                          {
                              Id = whiteppr.Id,
                              Title = whiteppr.Title,
                              imageName = whiteppr.ContentImage,
                              Description = whiteppr.Discription,
                              ContentType = whiteppr.ContentType,
                              ResourceType = whiteppr.Resourcetype
                          });
            return result;
        }

        public string getEnumString(Contents contents)
        {
            var str = string.Empty;
            switch (contents)
            {
                case Contents.WhitePaper:
                    str = "White Paper";
                    break;
                case Contents.Article:
                    str = "Article";
                    break;
                case Contents.Report:
                    str = "Report";
                    break;
                case Contents.Datasheet:
                    str = "Data Sheet";
                    break;
                case Contents.CaseStudy:
                    str = "Case Study";
                    break;
                default:
                    break;
            }
            return str;
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
