﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{
    public class PlatformRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        private string loginUserId { get; set; }
        public PlatformRepository()
        {
            _ctx = new ApplicationDbContext();
        }
        public PlatformRepository(string loginUserId)
        {
            _ctx = new ApplicationDbContext();
            this.loginUserId = loginUserId;
        }
        public async Task<ResponseModel<object>> AddorUpdate(PlatformModel model)
        {
            if (!model.Url.Contains("http://") || !model.Url.Contains("https://"))
            {
                model.Url = "http://" + model.Url;
            }
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = string.Format("{0} Platform {1}", model.name, "Record update!")
            };
            var entity = await _ctx.Platform.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (entity == null)
            {
                entity = new Platform();
                entity.Name = model.name;
                entity.CountryId = model.CountryId;
                entity.Url = model.Url;
                _ctx.Platform.Add(entity);
                mResult.Message = string.Format("{0} Platform {1}", model.name, "Record Added");
            }
            else
            {
                entity.Name = model.name;
                entity.CountryId = model.CountryId;
                entity.Url = model.Url;
            }
            await _ctx.SaveChangesAsync();
            return mResult;
        }
        public async Task<ResponseModel<object>> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.Platform.FirstOrDefaultAsync(x => x.Id == Id);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("{0} Platform {1}", entity.Name, "record deleted");

                _ctx.Platform.Remove(entity);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }
        public async Task<PlatformModel> GetPlatform(int Id)
        {
            return await (from x in _ctx.Platform
                          where x.Id == Id
                          select new PlatformModel
                          {
                              Id = x.Id,
                              name = x.Name,
                              CountryId = x.CountryId,
                              Url = x.Url
                          }).FirstOrDefaultAsync();
        }
        public IQueryable<PlatformViewModel> GetPlatform()
        {
            return (from x in _ctx.Platform
                    select new PlatformViewModel
                    {
                        Id = x.Id,
                        name = x.Name + "-" + x.Country.Name,
                        countryName = x.Country.Name,
                        Url = x.Url
                    }).OrderBy(c => c.name);
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
