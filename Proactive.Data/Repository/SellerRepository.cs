﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{
    public class SellerRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        public SellerRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public List<GetSellerListModel> GetAllSellers()
        {
            return (from slr in _ctx.Sellers
                    select new GetSellerListModel
                    {
                        SellerId = slr.Id,
                        Name = slr.Name,
                        Contact = slr.ContactDetail,
                        SellerAccounts = slr.SellerAccounts.Select(x => new SellerAccountsList
                        {
                            Id = x.Id,
                            LinkedAccName = x.LinkedAccountName
                        }).ToList(),
                        SellerHighriskAccList = slr.SellerHighriskAccount.Select(x => new SellerHighriskAccList
                        {
                            Id = x.Id,
                            HighriskAccName = x.HighriskAccountName
                        }).ToList()
                    }).OrderBy(x => x.Name).ToList();
        }

        public List<GetRepeatedInfregment> GetRepeatedInfregment(string clientId)
        {
            List<GetRepeatedInfregment> model = new List<GetRepeatedInfregment>();
            try
            {

                model = (from inf in _ctx.Infringement
                         where inf.SellerId != null
                         && inf.ClientId == clientId
                         group inf by inf.PlatformId into pg
                         select new GetRepeatedInfregment
                         {
                             SellerId = pg.FirstOrDefault().Seller.Id,
                             Name = pg.FirstOrDefault().Seller.Name,
                             ContactDetail = pg.FirstOrDefault().Seller.ContactDetail,
                             SellerAccounts = pg.FirstOrDefault().Seller.SellerAccounts.Select(x => new SellerAccountsList
                             {
                                 Id = x.Id,
                                 LinkedAccName = x.LinkedAccountName
                             }).ToList(),
                             SellerHighriskAccList = pg.FirstOrDefault().Seller.SellerHighriskAccount.Select(x => new SellerHighriskAccList
                             {
                                 Id = x.Id,
                                 HighriskAccName = x.HighriskAccountName
                             }).ToList(),
                             PlateForm = pg.FirstOrDefault().Platform.Name,
                             Country = pg.FirstOrDefault().Platform.Country.Name,
                             Count = pg.Count()
                         }).ToList();

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return model;

        }


        public async Task<ResponseModel<object>> AddUpdateSeller(AddSellerModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (model.HighriskAccList.Count() > 0)
                    model.IsHighriskAcc = true;
                else
                    model.IsHighriskAcc = false;

                if (model.LinledAccList.Count() > 0)
                    model.IsLinkedAcc = true;
                else
                    model.IsLinkedAcc = false;

                bool isNew = false;
                Sellers objseller = await _ctx.Sellers.FirstOrDefaultAsync(x => x.Id == model.sellerId);
                if (objseller == null)
                {
                    isNew = true;
                    objseller = new Sellers();
                }
                objseller.Name = model.Name;
                objseller.ContactDetail = model.Contact;
                if (model.LinledAccList.Count() > 0)
                {
                    objseller.IsLinkedAccount = true;
                }

                if (model.HighriskAccList.Count() > 0)
                {
                    objseller.IsHighriskAccount = true;
                }

                if (isNew)
                {
                    List<SellerAccounts> SellerAccList = new List<SellerAccounts>();
                    foreach (SellerAccountsList item in model.LinledAccList)
                    {
                        if (!string.IsNullOrWhiteSpace(item.LinkedAccName))
                        {
                            SellerAccList.Add(new SellerAccounts
                            {
                                LinkedAccountName = item.LinkedAccName,
                                Id = objseller.Id
                            });
                        }
                    }

                    List<SellerHighriskAccount> SellerHighriskAccList = new List<SellerHighriskAccount>();
                    foreach (SellerHighriskAccList item in model.HighriskAccList)
                    {
                        if (!string.IsNullOrWhiteSpace(item.HighriskAccName))
                        {
                            SellerHighriskAccList.Add(new SellerHighriskAccount
                            {
                                HighriskAccountName = item.HighriskAccName,
                                Id = objseller.Id
                            });
                        }
                    }

                    objseller.SellerAccounts = SellerAccList;
                    objseller.SellerHighriskAccount = SellerHighriskAccList;
                    _ctx.Sellers.Add(objseller);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Seller Added successfully";
                }
                else
                {
                    _ctx.SellerAccounts.RemoveRange(_ctx.SellerAccounts.Where(c => c.SellerId == model.sellerId));
                    await _ctx.SaveChangesAsync();

                    _ctx.SellerHighriskAccounts.RemoveRange(_ctx.SellerHighriskAccounts.Where(c => c.SellerId == model.sellerId));
                    await _ctx.SaveChangesAsync();

                    List<SellerAccounts> soonghashtag = new List<SellerAccounts>();
                    foreach (SellerAccountsList item in model.LinledAccList)
                    {
                        if (!string.IsNullOrWhiteSpace(item.LinkedAccName))
                        {
                            soonghashtag.Add(new SellerAccounts
                            {
                                LinkedAccountName = item.LinkedAccName,
                                SellerId = objseller.Id
                            });
                        }
                    }

                    List<SellerHighriskAccount> HiriskAccountList = new List<SellerHighriskAccount>();
                    foreach (SellerHighriskAccList item in model.HighriskAccList)
                    {
                        if (!string.IsNullOrWhiteSpace(item.HighriskAccName))
                        {
                            HiriskAccountList.Add(new SellerHighriskAccount
                            {
                                HighriskAccountName = item.HighriskAccName,
                                SellerId = objseller.Id
                            });
                        }
                    }
                    objseller.SellerAccounts = soonghashtag;
                    objseller.SellerHighriskAccount = HiriskAccountList;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Seller update successfully";
                }
                await _ctx.SaveChangesAsync();

            }
            catch (DbEntityValidationException e)
            {
                foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;

        }

        public async Task<AddSellerModel> GetSellerDetail(int id)
        {
            return await (from slr in _ctx.Sellers
                          where slr.Id == id
                          select new AddSellerModel
                          {
                              sellerId = slr.Id,
                              Name = slr.Name,
                              Contact = slr.ContactDetail,
                              IsHighriskAcc = slr.IsHighriskAccount,
                              IsLinkedAcc = slr.IsLinkedAccount,
                              LinledAccList = slr.SellerAccounts.Select(x => new SellerAccountsList
                              {
                                  Id = x.Id,
                                  LinkedAccName = x.LinkedAccountName

                              }).ToList(),
                              HighriskAccList = slr.SellerHighriskAccount.Select(x => new SellerHighriskAccList
                              {
                                  Id = x.Id,
                                  HighriskAccName = x.HighriskAccountName

                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                Sellers data = await _ctx.Sellers.FirstOrDefaultAsync(x => x.Id == Id);

                if (data != null)
                {
                    if (_ctx.Infringement.Any(x => x.SellerId == Id))
                    {
                        mResult.Message = "Seller Used on infrigment, so you cant delete this user!";
                    }
                    else
                    {
                        List<SellerAccounts> ChildAcclist = await _ctx.SellerAccounts.Where(x => x.SellerId == Id).ToListAsync();
                        _ctx.SellerAccounts.RemoveRange(ChildAcclist);

                        List<SellerHighriskAccount> ChildHighriskAcclist = await _ctx.SellerHighriskAccounts.Where(x => x.SellerId == Id).ToListAsync();

                        _ctx.SellerHighriskAccounts.RemoveRange(ChildHighriskAcclist);
                        _ctx.Sellers.Remove(data);
                        await _ctx.SaveChangesAsync();
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Seller deleted successfully!";
                    }
                }
                else
                {
                    mResult.Message = "No Seller found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public IQueryable<GetSellerListModel> GetSellers()
        {
            return (from x in _ctx.Sellers
                    select new GetSellerListModel
                    {
                        SellerId = x.Id,
                        Name = x.Name
                    }).OrderBy(x => x.Name);
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
