﻿using LinqKit;
using Newtonsoft.Json;
using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{
    public class InfringementRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;

        private string loginUserId { get; set; }

        public InfringementRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public InfringementRepository(string loginUserId)
        {
            _ctx = new ApplicationDbContext();
            this.loginUserId = loginUserId;
        }

        public async Task<ResponseModel<object>> AddorUpdate(InfringementAddModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = string.Format("{0} Infringement {1}", model.Title, "Updated")
            };
            var entity = await _ctx.Infringement.FirstOrDefaultAsync(x => x.Id == model.InfringementId);
            if (entity == null)
            {
                entity = new Infringement()
                {
                    InfringementImage = new List<InfringementImage>()
                };
                entity.ReviewStatus = model.IsSendForRview ? ReviewStatus.Pending : ReviewStatus.Open;
                mResult.Message = string.Format("{0} Infringement {1}", model.Title, "record added successfully");
            }
            else
            {
                entity.UpdatedDate = Utility.GetSystemDateTimeUTC();
            }
            entity.ClientId = model.ClientId;
            entity.PlatformId = model.PlatformId;
            entity.InfringementTypeId = model.InfringementTypeId;
            entity.ItemLocation = model.ItemLocation;
            entity.SellerLocation = model.SellerLocation;
            entity.Title = model.Title;
            entity.Url = model.Url;
            entity.UserId = loginUserId;
            entity.ItemNumber = model.ItemNumber;
            entity.PlatformId = model.PlatformId;
            entity.Price = model.Price;
            entity.Quantity = model.Quantity;
            entity.Remark = model.Remark;
            entity.UserId = model.UserId;
            entity.SellerId = model.SellerId;
            entity.IsSendForRview = model.IsSendForRview;
            if (model.InfringementId == 0)
                _ctx.Infringement.Add(entity);

            if (!string.IsNullOrEmpty(model.imageObject))
            {
                List<ImagesModel> imageList = JsonConvert.DeserializeObject<List<ImagesModel>>(model.imageObject);
                foreach (ImagesModel item in imageList)
                {
                    entity.InfringementImage.Add(new InfringementImage
                    {
                        ImageName = item.imageName,
                        InfringementId = entity.Id,
                    });
                }
            }
            try
            {
                await _ctx.SaveChangesAsync();

            }
            catch (Exception EX)
            {
                mResult.Message = EX.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.Infringement.FirstOrDefaultAsync(x => x.Id == Id);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("{0} Infringement {1}", entity.Title, "record deleted");

                _ctx.Infringement.Remove(entity);
                try
                {
                    await _ctx.SaveChangesAsync();
                }
                catch (Exception err)
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = err.Message;
                }
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> RetractDelete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.Infringement.FirstOrDefaultAsync(x => x.Id == Id);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("{0} Infringement {1}", entity.Title, "record deleted");
                entity.IsDelete = true;
                try
                {
                    await _ctx.SaveChangesAsync();
                }
                catch (Exception err)
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = err.Message;
                }
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> RemoveImage(int InfringementId, string ImageName)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.InfringementImage.FirstOrDefaultAsync(x => x.ImageName == ImageName && x.InfringementId == InfringementId);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("Infringement image {0}", "deleted");

                _ctx.InfringementImage.Remove(entity);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public IQueryable<InfringementViewModel> GetInfringement()
        {
            return (from x in _ctx.Infringement
                    where !x.IsDelete
                    let InfringementImage = x.InfringementImage.FirstOrDefault()
                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name,
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name + "-" + x.Platform.Country.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        IsRepeated = _ctx.Infringement.Count(y => y.PlatformId == x.PlatformId && y.ClientId == x.ClientId) > 1 ? true : false,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = z.CeaseAndDesist.FileName,
                                                       imageUrl = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetInfringement(string UserId)
        {
            return (from x in _ctx.Infringement
                    where !x.IsDelete
                    && x.ClientId == UserId
                    let InfringementImage = x.InfringementImage.FirstOrDefault()
                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name,
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Country = x.Platform.Country.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        IsRepeated = _ctx.Infringement.Count(y => y.PlatformId == x.PlatformId && y.ClientId == x.ClientId) > 1 ? true : false,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = z.CeaseAndDesist.FileName,
                                                       imageUrl = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetInfringementReview(string UserId)
        {
            return (from x in _ctx.Infringement
                    where !x.IsDelete
                    && x.ClientId == UserId
                    && x.IsSendForRview
                    && x.ReviewStatus == ReviewStatus.Pending
                    let InfringementImage = x.InfringementImage.FirstOrDefault()
                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Client.FirstName,
                        InfringementType = x.InfringementType.Name,
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        Country = x.Platform.Country.Name,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = z.CeaseAndDesist.FileName,
                                                       imageUrl = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetInfringementApprove(string UserId)
        {
            return (from x in _ctx.Infringement
                    where !x.IsDelete
                    && x.ClientId == UserId
                    && x.IsSendForRview
                    && x.ReviewStatus == ReviewStatus.Approved
                    let InfringementImage = x.InfringementImage.FirstOrDefault()
                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name,
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        Country = x.Platform.Country.Name,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = z.CeaseAndDesist.FileName,
                                                       imageUrl = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetInfringementReject(string UserId)
        {
            return (from x in _ctx.Infringement
                    where !x.IsDelete
                    && x.ClientId == UserId
                    //&& x.IsSendForRview
                    && x.ReviewStatus == ReviewStatus.Reject
                    let InfringementImage = x.InfringementImage.FirstOrDefault()
                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name,
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        Country = x.Platform.Country.Name,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = z.CeaseAndDesist.FileName,
                                                       imageUrl = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetInfringementByStatus(ReviewStatus status, string UserId)
        {
            return (from x in _ctx.Infringement
                    where !x.IsDelete
                    && x.ClientId == UserId
                    //&& x.IsSendForRview
                    && x.ReviewStatus == status
                    let InfringementImage = x.InfringementImage.FirstOrDefault()
                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name,
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        Country = x.Platform.Country.Name,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = z.CeaseAndDesist.FileName,
                                                       imageUrl = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetInfringementRetract(string UserId)
        {
            return (from x in _ctx.Infringement
                    where !x.IsDelete
                    && x.ClientId == UserId
                    && x.ReviewStatus == ReviewStatus.RetractByClient
                    let InfringementImage = x.InfringementImage.FirstOrDefault()
                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name,
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        Country = x.Platform.Country.Name,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        Reason = x.RetractReason,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = z.CeaseAndDesist.FileName,
                                                       imageUrl = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetInfringementByFilter(InfringementFilter model)
        {
            return (from x in _ctx.Infringement
                    where (model.StartDate == null || x.CreatedDate >= model.StartDate)
                    && (model.EndDate == null || x.CreatedDate <= model.EndDate)
                    && (model.ClientId == null || x.ClientId == model.ClientId)
                    && (model.PlatformId == null || x.PlatformId == model.PlatformId)
                    && (model.InfringementTypeId == null || x.InfringementTypeId == model.InfringementTypeId)
                    && (model.ReviewStatus == null || x.ReviewStatus == model.ReviewStatus)
                    && !x.IsDelete
                    && (x.InfringementType.Name.ToString().Contains(model.SearchText)
                    || x.Title.Contains(model.SearchText)
                    || x.Client.FirstName.Contains(model.SearchText)
                    || x.ItemNumber.Contains(model.SearchText)
                    || x.SellerLocation.Contains(model.SearchText)
                    || model.SearchText == null
                    || model.SearchText == string.Empty)
                    let InfringementImage = x.InfringementImage.FirstOrDefault()

                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name.ToString(),
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       imageUrl = z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public IQueryable<InfringementViewModel> GetClientInfringementByFilter(InfringementFilter model, string ClientId)
        {
            return (from x in _ctx.Infringement
                    where x.ClientId == ClientId
                    where (model.StartDate == null || x.CreatedDate >= model.StartDate)
                    && (model.EndDate == null || x.CreatedDate <= model.EndDate)
                    //&& (model.ClientId == null || x.ClientId == model.ClientId)
                    && (model.PlatformId == null || x.PlatformId == model.PlatformId)
                    && (model.InfringementTypeId == null || x.InfringementTypeId == model.InfringementTypeId)
                    && (model.ReviewStatus == null || x.ReviewStatus == model.ReviewStatus)
                    && !x.IsDelete
                    && (x.InfringementType.Name.ToString().Contains(model.SearchText)
                    || x.Title.Contains(model.SearchText)
                    || x.Client.FirstName.Contains(model.SearchText)
                    || x.ItemNumber.Contains(model.SearchText)
                    || x.SellerLocation.Contains(model.SearchText)
                    || model.SearchText == null
                    || model.SearchText == string.Empty)
                    let InfringementImage = x.InfringementImage.FirstOrDefault()

                    select new InfringementViewModel
                    {
                        InfringementId = x.Id,
                        Title = x.Title,
                        ClientName = x.Seller.Name,
                        InfringementType = x.InfringementType.Name.ToString(),
                        ItemLocation = x.ItemLocation,
                        ItemNumber = x.ItemNumber,
                        Platform = x.Platform.Name,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Remark = x.Remark,
                        SellerLocation = x.SellerLocation,
                        Url = x.Url,
                        UserId = x.UserId,
                        ReviewStatus = x.ReviewStatus,
                        ReportedTime = x.CreatedDate,
                        RemovedTime = x.ReviewStatus == ReviewStatus.Close ? x.UpdatedDate : null,
                        ImageUrl = GlobalConfig.InfringementUrl + (InfringementImage != null ? InfringementImage.ImageName : GlobalConfig.defaultImageName),
                        imageList = (from y in x.InfringementImage
                                     select new ImagesModel
                                     {
                                         imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                         imageName = y.ImageName
                                     }).ToList(),
                        CeaseandDesistImageList = (from z in _ctx.InfringementCeaseAndDesist
                                                   where z.InfringementId == x.Id
                                                   select new CeaseandDesistImagesModel
                                                   {
                                                       imageName = GlobalConfig.CeaseAndDesistUrl + z.CeaseAndDesist.FileName,
                                                       imageUrl = z.CeaseAndDesist.FileName,
                                                       Source = z.Source
                                                   }).ToList()
                    }).OrderByDescending(c => c.InfringementId);
        }

        public async Task<InfringementAddModel> GetInfringement(int Id)
        {
            return await (from x in _ctx.Infringement
                          where x.Id == Id
                          && !x.IsDelete
                          let InfringementImage = x.InfringementImage.FirstOrDefault()
                          select new InfringementAddModel
                          {
                              InfringementId = x.Id,
                              Title = x.Title,
                              ClientId = x.ClientId,
                              InfringementTypeId = x.InfringementTypeId,
                              ItemLocation = x.ItemLocation,
                              ItemNumber = x.ItemNumber,
                              PlatformId = x.PlatformId,
                              Price = x.Price,
                              Quantity = x.Quantity,
                              Remark = x.Remark,
                              SellerLocation = x.SellerLocation,
                              Url = x.Url,
                              SellerId = x.SellerId ?? 0,
                              UserId = x.UserId,
                              IsSendForRview = x.IsSendForRview,
                              imageList = (from y in x.InfringementImage
                                           select new ImagesModel
                                           {
                                               imageUrl = GlobalConfig.InfringementUrl + y.ImageName,
                                               imageName = y.ImageName
                                           }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<string>> GetInfringementImage(int Id)
        {
            return await (from x in _ctx.InfringementImage
                          where x.InfringementId == Id
                          select new
                          {
                              imageUrl = GlobalConfig.InfringementUrl + x.ImageName,
                          }).Select(x => x.imageUrl).ToListAsync();
        }

        public ResponseModel<object> CloseSelectedInfringement(string selectedInfringementIds, ReviewStatus Status)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            if (selectedInfringementIds != null)
            {
                List<string> objIdList = selectedInfringementIds != null ? selectedInfringementIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
                List<int> intList = objIdList.ConvertAll(int.Parse);
                _ctx.Infringement.Where(x => intList.Contains(x.Id)).ToList().ForEach(x => { x.ReviewStatus = Status; x.UpdatedDate = Utility.GetSystemDateTimeUTC(); });
                _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("{0} record {1}", intList.Count(), "Closed successfully");
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public ResponseModel<object> DeleteSelectedInfringement(string selectedInfringementIds)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            if (selectedInfringementIds != null)
            {
                List<string> objIdList = selectedInfringementIds != null ? selectedInfringementIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
                List<int> intList = objIdList.ConvertAll(int.Parse);
                var entity = _ctx.Infringement.Where(x => intList.Contains(x.Id)).ToList();
                _ctx.Infringement.RemoveRange(entity);
                _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("{0} record {1}", intList.Count(), "delete successfully");
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ClosedToggle(int Id, ReviewStatus Status)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var objInfringement = await _ctx.Infringement.FirstOrDefaultAsync(x => x.Id == Id);
            if (objInfringement != null)
            {
                objInfringement.ReviewStatus = Status;
                objInfringement.UpdatedDate = Utility.GetSystemDateTimeUTC();
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                if (objInfringement.ReviewStatus == ReviewStatus.Close)
                    mResult.Message = "Closed successfully";
                else
                    mResult.Message = "Open successfully";
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> RetractByClient(int Id, string Reason, ReviewStatus Status)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var objInfringement = await _ctx.Infringement.FirstOrDefaultAsync(x => x.Id == Id);
            if (objInfringement != null)
            {
                objInfringement.RetractReason = Reason;
                objInfringement.ReviewStatus = Status;
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                if (objInfringement.ReviewStatus == ReviewStatus.RetractByClient)
                    mResult.Message = "Retrected successfully";
                else
                    mResult.Message = "Close successfully";
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AddUpdateCeaseAndDesist(string FileName, string selectedInfringementIds, string IsClose, string Source)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = string.Format("Ceases And Desist added")
            };

            try
            {
                var ceaseanddesist = new CeaseAndDesist()
                {
                    FileName = FileName
                };
                _ctx.CeaseAndDesist.Add(ceaseanddesist);
                await _ctx.SaveChangesAsync();
                if (selectedInfringementIds != null)
                {
                    List<string> objIdList = selectedInfringementIds != null ? selectedInfringementIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
                    List<int> intList = objIdList.ConvertAll(int.Parse);
                    if (IsClose == "true")
                        _ctx.Infringement.Where(x => intList.Contains(x.Id)).ToList().ForEach(x => { x.ReviewStatus = ReviewStatus.Close; });
                    List<InfringementCeaseAndDesist> transaction = new List<InfringementCeaseAndDesist>();
                    foreach (var item in intList)
                    {
                        transaction.Add(new InfringementCeaseAndDesist
                        {
                            InfringementId = item,
                            CeaseAndDesistId = ceaseanddesist.Id,
                            Source = Source
                        });
                    }
                    _ctx.InfringementCeaseAndDesist.AddRange(transaction);
                    await _ctx.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {

                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
