﻿using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{
    public class UtilityRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        public UtilityRepository()
        {
            _ctx = new ApplicationDbContext();
        }
        public async Task<List<ChartViewModel>> PlatformChartReports(string UserId)
        {
            List<ChartViewModel> model = new List<ChartViewModel>();
            try
            {
                var objInfringement = await _ctx.Infringement.Where(p => p.ClientId == UserId || p.UserId == UserId).ToListAsync();
                var objChardata = objInfringement.GroupBy(w => w.Platform.Name).Select(g => new ChartViewModel
                {
                    label = g.Key,
                    value = g.Count(),
                    close = g.Count(x => x.ReviewStatus == ReviewStatus.Close || x.ReviewStatus == ReviewStatus.Reject),
                    open = g.Count(x => x.ReviewStatus == ReviewStatus.Open || x.ReviewStatus == ReviewStatus.Approved),
                    pending = g.Count(x => x.ReviewStatus == ReviewStatus.Pending),
                    retractbyclient = g.Count(x => x.ReviewStatus == ReviewStatus.RetractByClient)
                    //link = GlobalConfig.InfringementChartUrl + "?Name=" + g.Key
                }).ToList();
                return objChardata;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return model;

        }
        public async Task<DatasetModel> PlatformBarChartReports(string UserId)
        {
            DatasetModel model = new DatasetModel();
            List<Platform> platforms = await _ctx.Platform.Where(x => x.Infringement.Count > 0).ToListAsync();
            model.barchartlist = (from x in platforms
                                      //group x by x.Name into g
                                  select new ChartViewModel
                                  {
                                      label = x.Name + " " + " " + x.Country.Name,
                                      value = x.Infringement.Count(),
                                      close = x.Infringement.Count(y => (y.ReviewStatus == ReviewStatus.Close || y.ReviewStatus == ReviewStatus.Reject) && (y.ClientId == UserId || y.UserId == UserId)),
                                      open = x.Infringement.Count(y => (y.ReviewStatus == ReviewStatus.Close || y.ReviewStatus == ReviewStatus.Reject) && (y.ClientId == UserId || y.UserId == UserId))
                                  }).ToList();
            List<DatasetModel> datasetList = new List<DatasetModel>();
            List<DataModel> dataset = new List<DataModel>();

            dataset.Add(new DataModel()
            {
                seriesname = "Reject",
                data = (from x in platforms
                            //group x by x.Name into g
                        select new DataSetValueModel
                        {
                            //value = g.FirstOrDefault().Infringement.Count(y => y.ReviewStatus == ReviewStatus.Reject && (y.ClientId == UserId || y.UserId == UserId))
                            value = x.Infringement.Count(y => y.ReviewStatus == ReviewStatus.Reject && (y.ClientId == UserId || y.UserId == UserId))
                        }).ToList()
            });

            dataset.Add(new DataModel()
            {
                //seriesname = "Client review",
                seriesname = "Open",
                data = (from x in platforms
                            //group x by x.Name into g
                        select new DataSetValueModel
                        {
                            //value = g.FirstOrDefault().Infringement.Count(y => (y.ReviewStatus == ReviewStatus.Open || y.ReviewStatus == ReviewStatus.Approved) && y.IsSendForRview && (y.ClientId == UserId || y.UserId == UserId))
                            value = x.Infringement.Count(y => y.ReviewStatus == ReviewStatus.Open && (y.ClientId == UserId || y.UserId == UserId))
                        }).ToList()
            });

            dataset.Add(new DataModel()
            {
                //seriesname = "Client review",
                seriesname = "Retracted",
                data = (from x in platforms
                            //group x by x.Name into g
                        select new DataSetValueModel
                        {
                            //value = g.FirstOrDefault().Infringement.Count(y => y.ReviewStatus == ReviewStatus.RetractByClient && y.IsSendForRview && (y.ClientId == UserId || y.UserId == UserId))
                            value = x.Infringement.Count(y => y.ReviewStatus == ReviewStatus.RetractByClient && (y.ClientId == UserId || y.UserId == UserId))
                        }).ToList()
            });

            datasetList.Add(new DatasetModel
            {
                dataset = dataset
            });

            model.dataset = dataset;
            return model;
        }

        public async Task<SummaryModel> SummaryDetail()
        {
            SummaryModel model1 = new SummaryModel();
            var group = await _ctx.Infringement.ToListAsync();
            model1.Total = group.Count();
            model1.close = group.Count(x => x.ReviewStatus == ReviewStatus.Close);
            model1.Open = group.Count(x => x.ReviewStatus == ReviewStatus.Open);
            model1.Rejected = group.Count(x => x.ReviewStatus == ReviewStatus.Reject);
            model1.pending = group.Count(x => x.ReviewStatus == ReviewStatus.Pending);
            model1.retracted = group.Count(x => x.ReviewStatus == ReviewStatus.RetractByClient);
            model1.Approved = group.Count(x => x.ReviewStatus == ReviewStatus.Approved);

            return model1;
        }

        public async Task<SummaryModel> SummaryDetailByDate(DateTime date, DateTime endDate)
        {
            SummaryModel model1 = new SummaryModel();
            var group = await _ctx.Infringement.Where(x => x.CreatedDate >= date && x.CreatedDate <= endDate).ToListAsync();
            model1.Total = group.Count();
            model1.close = group.Count(x => x.ReviewStatus == ReviewStatus.Close);
            model1.Open = group.Count(x => x.ReviewStatus == ReviewStatus.Open);
            model1.Rejected = group.Count(x => x.ReviewStatus == ReviewStatus.Reject);
            model1.pending = group.Count(x => x.ReviewStatus == ReviewStatus.Pending);
            model1.retracted = group.Count(x => x.ReviewStatus == ReviewStatus.RetractByClient);
            model1.Approved = group.Count(x => x.ReviewStatus == ReviewStatus.Approved);

            return model1;
        }

        public async Task<List<ChartViewModel>> ChartReportByPlatform(string Name, string UserId)
        {
            var objInfringement = await _ctx.Infringement.Where(p => p.Platform.Name == Name && (p.ClientId == UserId || p.UserId == UserId)).ToListAsync();
            var objChardata = objInfringement.GroupBy(w => w.InfringementType.Name).Select(g => new ChartViewModel
            {
                label = g.Key.ToString(),
                value = g.Count(),
                open = g.Where(x => x.ReviewStatus == ReviewStatus.Open).Count(),
                close = g.Where(x => x.ReviewStatus == ReviewStatus.Close).Count(),
                pending = g.Where(x => x.ReviewStatus == ReviewStatus.Pending).Count(),
                retractbyclient = g.Where(x => x.ReviewStatus == ReviewStatus.RetractByClient).Count(),
            }).ToList();
            return objChardata;
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
