﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Entities;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Data.Repository
{
    public class InsightsTrendsRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        public InsightsTrendsRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<InsightsTrendsModel> GetInsightsTrends()
        {
            return (from x in _ctx.InsightsAndTrends
                    select new InsightsTrendsModel
                    {
                        Id = x.Id,
                        Description = x.Description,
                        IsViewOnWeb = x.IsViewOnWeb,
                        imageName = x.ImageName,
                        Title = x.Title
                    }).OrderByDescending(c => c.Id);
        }

        public IQueryable<InsightsTrendsModel> GetInsightsTrends(string UserId)
        {
            return (from x in _ctx.ClientInsightsAndTrends
                    where x.ClientId == UserId
                    select new InsightsTrendsModel
                    {
                        Id = x.Id,
                        Description = x.InsightsAndTrends.Description,
                        Title = x.InsightsAndTrends.Title,
                        imageName = x.InsightsAndTrends.ImageName
                    }).OrderByDescending(c => c.Id);
        }


        public async Task<ResponseModel<object>> AddorUpdate(InsightsTrendsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = string.Format("Insights Trends Record update!")
            };
            var entity = await _ctx.InsightsAndTrends.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (entity == null)
            {
                entity = new InsightsAndTrends();
                entity.Description = model.Description;
                entity.Title = model.Title;
                entity.IsViewOnWeb = true;
                entity.ImageName = model.imageName;
                _ctx.InsightsAndTrends.Add(entity);
                mResult.Message = string.Format("Insights Trends Record added!");

            }
            else
            {
                entity.ImageName = model.imageName;
                entity.Description = model.Description;
                entity.IsViewOnWeb = model.IsViewOnWeb;
                entity.IsViewOnWeb = true;
                entity.Title = model.Title;
                entity.IsViewOnWeb = model.IsViewOnWeb;
                _ctx.ClientInsightsAndTrends.RemoveRange(_ctx.ClientInsightsAndTrends.Where(c => c.InsightsAndTrendsId == entity.Id));
            }

            await _ctx.SaveChangesAsync();
            return mResult;
        }

        public async Task<InsightsTrendsModel> GetInsightsTrends(int Id)
        {
            InsightsTrendsModel model = new InsightsTrendsModel();
            var objData = await (from x in _ctx.InsightsAndTrends
                                 where x.Id == Id
                                 select new
                                 {
                                     Id = x.Id,
                                     Description = x.Description,
                                     Title = x.Title,
                                     ClientList = x.ClientInsightsAndTrends.Select(y => y.ClientId).ToList(),
                                     ImageName = x.ImageName,
                                     x.IsViewOnWeb
                                 }).FirstOrDefaultAsync();
            if (objData != null)
            {
                foreach (var item in _ctx.Users.Where(x => x.UserRole == Role.Client).ToList().OrderBy(x => x.Id).ToList())
                {
                    model.ClientList.Add(new CheckBoxModel() { Value = item.Id, Text = item.FirstName, IsChecked = objData.ClientList.Contains(item.Id) });
                }
                model.Id = objData.Id;
                model.IsViewOnWeb = objData.IsViewOnWeb;
                model.Description = objData.Description;
                model.Title = objData.Title;
                model.imageName = objData.ImageName;
            }
            return model;
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>() { Status = ResponseStatus.Failed };
            var entity = await _ctx.InsightsAndTrends.FirstOrDefaultAsync(x => x.Id == Id);
            if (entity != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("Insights trends record deleted");

                _ctx.InsightsAndTrends.Remove(entity);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
