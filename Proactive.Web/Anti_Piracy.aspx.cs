﻿using Proactive.Core.Enumerations;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Cabling_Services : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BIndService();
        }
    }
    private void BIndService()
    {
        BrandServices _service = new BrandServices();
        var objServicesList = _service.GetContentServices(Resources.Antipiracy);
        if (objServicesList.Count > 0)
            rptServices.DataSource = objServicesList;
        else
            rptServices.DataSource = null;
        rptServices.DataBind();
    }
}