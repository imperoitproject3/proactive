﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="anti_piracy.aspx.cs" Inherits="Cabling_Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Anti Piracy services by Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/software.jpeg" />
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">

                        <h1 class="text-white">ANTI-PIRACY</h1>
                        <p class="lead text-white">
                            Fight against online piracy and unauthorized online distribution of your digital media.
                        </p>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>

        <section class="article-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-9">
                        <div class="article-body">
                            <p class="lead">
                                How we can help you tackle Piracy?
                            </p>
                            <p>
                                We protect your content against piracy by rapidly identifying & removing unauthorized copies, unauthorized online distribution of digital media & violation of licensing agreement.<br />
                                <br />
                                The solution ensures best strategies are in place for protecting your authorized distribution channels and helps you to prevent from financial losses due to piracy.
Providing an effective solution to accurately detect, verify and enforce against copyright infringements which especially relating to illegal cyber locker, P2P networks and streaming offers.<br />
                                <br />
                                This program helps to preserve customer confidence and purchasing your legitimate content, which increase your online sales revenues.
Delivers key insights into how piracy trends are developing and gives you complete picture of the online markets.


                            </p>





                            <ul class="list-group">

                                <li class="list-group-item">Copyrighted images, text & logo protection.

                                </li>
                                <li class="list-group-item">Protect all type of digital content – eBook’s, software, music, movie, video and illegal television broadcast.</li>
                                <li class="list-group-item">Detects the listing, content which are in breach of licensing agreements.</li>
                                <li class="list-group-item">Prevent unauthorized downloading and distribution of illegal content.</li>
                                <li class="list-group-item">Quickly identifying and enforce against copyright infringement, removing unauthorized copies.</li>
                            </ul>






                        </div>
                        <!--end of article body-->

                    </div>
                    <div class="col-sm-3 col-md-offset-1">
                        <div class="blog-sidebar">
                            <div class="sidebar-widget">
                                <asp:Repeater ID="rptServices" runat="server">
                                    <ItemTemplate>
                                        <h2><%# Eval("StrContentType") %></h2>
                                        <img src="<%# Page.ResolveUrl("~/Files/BrandImages/"+Eval("imageName")) %>" width="250px" height="150px"><br />
                                        <a href="<%# Page.ResolveUrl("~/services.aspx?resources="+Eval("ResourceType")+"&brand="+Eval("ContentType")+"&Id="+Eval("Id")) %>">
                                            <h4><%# Eval("Title") %></h4>
                                        </a>
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <%--<div class="col-sm-3 col-md-offset-1">
                        <div class="blog-sidebar">


                            <div class="sidebar-widget">
                                <h5>OUR SERVICES</h5>

                                <img src="img/Sales.jpg" /><br />
                                <br />
                                Avail our vast range of services, uniquely customized for your business needs.<br />
                                <br />
                                We have firm belief in catering to our clients strictly as per their specifications in order to facilitate their convenience in proficiently utilizing from the below services we offer.

                                <br />
                                <br />
                                <ul class="tags">
                                    <li><a href="Online_Brand_Protection.aspx">Online Brand Protection</a></li>
                                    <li><a href="Online_Market_Monitoring.aspx">Online Market Monitoring</a></li>
                                    <li><a href="Social_Media_Monitoring.aspx">Social Media Monitoring</a></li>
                                    <li><a href="IP_Monitoring.aspx">IP Monitoring</a></li>

                                </ul>


                            </div>


                        </div>
                    </div>--%>
                </div>
                <!--end of row-->

                <!--end of row-->

            </div>
            <!--end of container-->
        </section>

    </div>
</asp:Content>

