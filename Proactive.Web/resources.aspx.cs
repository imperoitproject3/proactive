﻿using Proactive.Core.Enumerations;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Brand_Protection : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Resources"]))
            {
                var Resources = Request.QueryString["Resources"];

                var resources = (Resources)Enum.Parse(typeof(Resources), Resources);
                BindData(resources);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
    }

    private void BindData(Resources resources)
    {
        BrandServices _service = new BrandServices();
        var objBrandProtectionList = _service.GetContentForWeb(resources);
        //var objBrandProtectionList = _service.GetContentList(contents, resources);
        ltlTitle.Text = resources.ToString();
        if (objBrandProtectionList.Count > 0)
            rptResource.DataSource = objBrandProtectionList;
        else
            rptResource.DataSource = null;
        rptResource.DataBind();
    }
    private void BindData(Resources resources, Contents contentType)
    {
        BrandServices _service = new BrandServices();
        var objBrandProtectionList = _service.GetContentList(contentType, resources);
        if (objBrandProtectionList.Count > 0)
            rptResource.DataSource = objBrandProtectionList;
        else
            rptResource.DataSource = null;
        rptResource.DataBind();
    }

    protected void rbltype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["Resources"]))
        {
            var Resources = Request.QueryString["Resources"];
            var resources = (Resources)Enum.Parse(typeof(Resources), Resources);

            if (rbltype.SelectedValue == "All")
                BindData(resources);
            else
            {
                var contentType = (Contents)Enum.Parse(typeof(Contents), rbltype.SelectedValue);
                BindData(resources, contentType);
            }
        }
    }
}