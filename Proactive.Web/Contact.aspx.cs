﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ImperoIT.Email objEmail = new ImperoIT.Email();
        string Name = txtName.Text;
        string Email = txtEmail.Text;
        string Phone = txtNumber.Text;
        string Message = txtSuggestion.Text;

        objEmail.SendEmail(Name + " with Mobile Number " + Phone + " and Email " + Email + " has contacted you on your website www.proactivechannel.com.com. His message is " + Message, "info@proactivechannel.com", "info@proactivechannel.com", "", "raza@Imperoit.com", "Inquiry from website");
        txtEmail.Text = txtName.Text = txtNumber.Text = txtSuggestion.Text = "";
        Page page = HttpContext.Current.Handler as Page;
        ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "alert('Thanks For your Inquiry. We shall get back to you soon');", true);
        objEmail = null;

    }
}