﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Services by Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/automation.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h1 class="text-white">
                            <asp:Literal ID="ltrPageTitle" runat="server"></asp:Literal></h1>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>
        <section class="article-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-9">
                        <div class="article-body">
                            <p class="lead">
                                <asp:Literal ID="ltlTitle" runat="server"></asp:Literal>
                            </p>
                            <asp:Image ID="ServiceImage" runat="server" Width="100%" class="img-thumbnail" alt="Cinque Terre" />
                            <p>
                                <asp:Literal ID="ltlDescription" runat="server"></asp:Literal>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-offset-1">
                        <div class="blog-sidebar">
                            <div class="sidebar-widget">
                                <asp:Repeater ID="rptServices" runat="server">
                                    <ItemTemplate>
                                        <h2><%# Eval("StrContentType") %></h2>
                                        <img src="<%# Page.ResolveUrl("~/Files/BrandImages/"+Eval("imageName")) %>" width="250px" height="150px"><br />
                                        <a href="<%# Page.ResolveUrl("~/services.aspx?resources="+Eval("ResourceType")+"&brand="+Eval("ContentType")+"&Id="+Eval("Id")) %>">
                                            <h4><%# Eval("Title") %></h4>
                                        </a>
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end of row-->
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
</asp:Content>

