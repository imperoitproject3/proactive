﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.IO;
namespace ImperoIT
{
    public class Email
    {
        public int SendEmail(string Mailcontent, string From, string To, string Cc,string Bcc, string Subject)
        {

            const string SERVER = "relay-hosting.secureserver.net";
            MailMessage oMail = new System.Web.Mail.MailMessage();
            oMail.From = From;
            oMail.To = To;
            oMail.Cc = Cc;
            oMail.Bcc = Bcc;
            oMail.Subject = Subject;
            
            oMail.BodyFormat = MailFormat.Html;
            oMail.Priority = MailPriority.High;
            oMail.Body = Mailcontent;
            SmtpMail.SmtpServer = SERVER;
            SmtpMail.Send(oMail);
            oMail = null;
            return 1;
        }
    }
}