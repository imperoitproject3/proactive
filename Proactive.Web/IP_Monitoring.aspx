﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IP_Monitoring.aspx.cs" Inherits="Cabling_Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>IP Monitoring services by Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/about.jpg" />
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                          <h1 class="text-white">IP MONITORING</h1>
                        <p class="lead text-white">
                           Combating unauthorized use of your trademarks, logos and Intellectual Property.</h1>
                      
                        </p>
                        
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>

        <section class="article-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-9">
                        <div class="article-body">
                            <p class="lead">
                                How our IP monitoring program works?
							
                            </p>

                            <p>
                            Whether its trademark or brand name misuse, inappropriate use of logo or unlawful importation of goods, we identify and enforce against the listings, content that infringe upon your intellectual property. <br /><br />
Protecting your brands IP including design rights and patent rights, trade dress, right of publicity, items sold in unauthorized condition plus more.
Parallel Import: We help you to protect your product line from illegal importation of goods without the consent of the brand owner.

                                <br /><br />
                                Market-leading monitoring and enforcement technology, combined with a specialized trademark and IP analysis and enforcement team, enables our clients to rapidly detect and enforce against the unauthorized use of brands across all online platforms

                            </p>










                        </div>
                        <!--end of article body-->

                    </div>

                    <div class="col-sm-3 col-md-offset-1">
                        <div class="blog-sidebar">


                            <div class="sidebar-widget">
                                <h5>OUR SERVICES</h5>
                               
                                  <img src="img/Sales.jpg" /><br /> <br />Avail our vast range of services, uniquely customized for your business needs.<br /><br />
                                We have firm belief in catering to our clients strictly as per their specifications in order to facilitate their convenience in proficiently utilizing from the below services we offer.

                                <br />  <br />
                                <ul class="tags">
                                    <li><a href="Anti_Piracy.aspx">Anti-Piracy</a></li>
                                    <li><a href="Online_Market_Monitoring.aspx">Online Market Monitoring</a></li>
                                    <li><a href="Social_Media_Monitoring.aspx">Social Media Monitoring</a></li>
                                    <li><a href="Online_Brand_Protection.aspx">Online Brand Protection</a></li>

                                </ul>

                              
                            </div>


                        </div>
                    </div>

                </div>
                <!--end of row-->

                <!--end of row-->

            </div>
            <!--end of container-->
        </section>

    </div>
</asp:Content>

