﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Blogs.aspx.cs" Inherits="Blogs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
       <title>PROACTIVE CHANNEL - Blogs</title>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#accordion").accordion({
                heightStyle: "content",
                collapsible: true
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/Blog.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h1 class="text-white space-bottom-medium">Blogs</h1>
                        <br />
                        <br />
                      <%--  <h3 style="color: white;">Traditional providers only scratch the surface.
                            <br />
                            We deliver comprehensive actionable brand protection intelligence with in-depth research, analysis which other might have missed and enforce them in the real time.</h3>--%>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                </div>
                <div id="accordion">
                    <asp:Repeater runat="server" ID="rptBlogs">
                        <ItemTemplate>
                               <h1 style="color: #e74c3c; font-size: 20px;"><%# Eval("Title")%></h1>
                            <div>
                                <span class="post-date">Published on <%# string.Format("{0:MMMM dd, yyyy}", Eval("Published")) %> by <%# Eval("Author")%></span>
                                <p><%# Eval("Description")%></p>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <br />
                </div>
            </div>
        </div>

    </div>
</asp:Content>

