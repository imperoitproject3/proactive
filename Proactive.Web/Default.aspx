﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>PROACTIVE CHANNEL - Global IP Protection</title>
    <style>
        .alt-font a {
            color: #333333 !important;
            font-weight: 400;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/homebanner.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 align="left" style="color: white;">Why Real time IP Enforcement is required for any brand?
                        </h2>
                        <h3 align="left">
                            <a href="who_we_are.aspx#WeAreAection" class="btn btn-primary btn-filled btn-xs">Learn more</a></h3>
                        <br />
                        <br />
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>
        <section class="duplicatable-content milestones">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">

                        <p class="lead">
                            Keeping your Brand always ahead in digital world by protecting entire brand continuum, strategies that deliver significant & measurable ROI & provides you with valuable insight which helps to prevent brand reputation erosion, loss of revenue, maintain product integrity and customer trust.
                        </p>
                    </div>
                </div>

            </div>

        </section>

        <section class="side-image clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 content col-sm-8 clearfix">
                        <h1 alighn="center">Services At Glance</h1>
                        <ul class="blog-snippet-2">
                            <li class="col-md-6">
                                <div class="icon">
                                    <i class="icon icon-global"></i>
                                </div>
                                <div class="title">
                                    <b class="redfont">ONLINE BRAND PROTECTION</b>
                                    <p>
                                        Delivering an impactful online brand protection program and security measure will drive and increase your online sales, providing enhancements to the integrity of the brand, reducing the threats that can impact your revenue and reputation .

                                            <span class="alt-font"><a href="Online_Brand_Protection.aspx">Read More..</a></span>
                                    </p>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="icon">
                                    <i class="icon icon-lock"></i>
                                </div>
                                <div class="title">
                                    <b class="redfont">ANTI-PIRACY</b>
                                    <p>
                                        A unique combination of proprietary technology, data intelligence and expertise create the most effective means for successfully removing unauthorized content and best strategies are in place for consistent protection of digital content.

                                            <span class="alt-font"><a href="Anti_Piracy.aspx">Read More..</a></span>
                                    </p>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="icon">
                                    <i class="icon icon-globe"></i>
                                </div>
                                <div class="title">
                                    <b class="redfont">ONLINE MARKET MONITORING</b>
                                    <p>
                                        Keeps you up-to-date on the critical developments and trends that are driving the global online markets.

                                            <span class="alt-font"><a href="Online_Market_Monitoring.aspx">Read More..</a></span>
                                    </p>
                                </div>
                            </li>

                            <li class="col-md-6">
                                <div class="icon">
                                    <i class="icon icon-genius"></i>
                                </div>
                                <div class="title">
                                    <b class="redfont">SOCIAL MEDIA MONITORING</b>
                                    <p>
                                        Continuous coverage and effective enforcement against the unauthorized activity - trademark and copyright infringement and distribution of counterfeit goods through this medium.
                                            <span class="alt-font"><a href="Social_Media_Monitoring.aspx">Read More..</a></span>
                                    </p>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="icon">
                                    <i class="icon icon-aperture"></i>
                                </div>
                                <div class="title">
                                    <b class="redfont">IP MONITORING</b>
                                    <p>
                                        Provides client with continuous insights, rapidly detect abuses as they occur then respond quickly and effective enforcement of IP
                                            <span class="alt-fonts"><a href="IP_Monitoring.aspx">Read More..</a></span>
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--end of row-->

                </div>
                <!--end of container-->
            </div>
        </section>

        <section class="duplicatable-content">
            <div class="container">
                <div class="row center-block">
                    <div class="col-md-12 text-center">
                        <h1>Trusted by the World’s High-end Brand’s.</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" align="center">
                        <img src="img/Images/100per.png" />
                        <br />
                        <p>
                            Success rate from identifying to removal.
                        </p>
                        <br />
                    </div>
                    <div class="col-md-3" align="center">
                        <img src="img/Images/fortune.png" />
                        <p>
                            Many of the companies use Proactive Channel to protect their brand online.
                        </p>
                        <br />
                    </div>
                    <div class="col-md-3" align="center">
                        <img src="img/Images/24-hours.png" />
                        <p>
                            Within 24hrs guarantee for removal of the infringement.
                        </p>
                        <br />
                    </div>
                    <div class="col-md-3" align="center">
                        <img src="img/Images/INTA.png" />
                        <p>
                            Registered member
                        </p>
                        <br />
                    </div>
                </div>
            </div>
        </section>

        <section class="duplicatable-content mobilehide">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>Why Proactive Channel?</h1>
                    </div>
                </div>
                <!--end of row-->

                <div class="row">

                    <!--end 6 col-->
                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-shield"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Commitment</h5>
                                <p>
                                    We are committed to protect your valuable brands online. Our industry expertise,
                                        <br />
                                    <span class="alt-font"><a href="About.aspx#content">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-linegraph"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Unrivaled Insights and trends</h5>
                                <p>
                                    We're constantly uncovering real time insights to give you the best possible picture of your brand in the world's online markets
                                        <br />
                                    <span class="alt-font"><a href="About.aspx#content">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->

                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-genius"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Best Practices</h5>
                                <p>
                                    Delivering the highest quality research, in-depth analysis and successful enforcement within the time which give you the accurate results.
                                       <br />
                                    <span class="alt-font"><a href="About.aspx#content">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--end 6 col-->

                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-piechart"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Enhanced Reports</h5>
                                <p>
                                    We help you to quantify the size and scope of illicit activity
                                       <br />
                                    <span class="alt-font"><a href="About.aspx#content">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-clock"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Deliver quick results</h5>
                                <p>
                                    We work 24/7 to keep clients one step ahead of all the potential risk and threats that Internet offers
                                        <br />
                                    <span class="alt-font"><a href="About.aspx#content">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->

                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-scope"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Get to the root of the problem</h5>
                                <p>
                                    We are committed to protect your valuable brands online. Our industry expertise,
                                        <br />
                                    <span class="alt-font"><a href="About.aspx#content">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->

                </div>
                <!--end of row-->
            </div>

        </section>

        <section class="duplicatable-content hidden-md hidden-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>Why Proactive Channel?</h1>
                    </div>
                </div>
                <!--end of row-->

                <div class="row">

                    <!--end 6 col-->
                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-shield"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Commitment</h5>
                                <p>
                                    We are committed to protect your valuable brands online. Our industry expertise,
                                        <br />
                                    <span class="alt-font"><a href="About.aspx">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-linegraph"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Unrivaled Insights and trends</h5>
                                <p>
                                    We're constantly uncovering real time insights to give you the best possible picture of your brand in the world's online markets
                                       <br />
                                    <span class="alt-font"><a href="About.aspx">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->
                    <div class="clearfix"></div>
                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-genius"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Best Practices</h5>
                                <p>
                                    Delivering the highest quality research, in-depth analysis and successful enforcement within the time which give you the accurate results.
                                       <br />
                                    <span class="alt-font"><a href="About.aspx">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--end 6 col-->

                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-piechart"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Enhanced Reports</h5>
                                <p>
                                    We help you to quantify the size and scope of illicit activity
                                       <br />
                                    <span class="alt-font"><a href="About.aspx">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-clock"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Deliver quick results</h5>
                                <p>
                                    We work 24/7 to keep clients one step ahead of all the potential risk and threats that Internet offers
                                       <br />
                                    <span class="alt-font"><a href="About.aspx">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!--end 6 col-->

                    <div class="col-sm-4">
                        <div class=" feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-scope"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Get to the root of the problem</h5>
                                <p>
                                    We are committed to protect your valuable brands online. Our industry expertise,
                                       <br />
                                    <span class="alt-font"><a href="About.aspx">Read More..</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->

                </div>
                <!--end of row-->
            </div>

        </section>

        <section class="col-md-12 text-center">
            <div class="container">
                <div class="row">
                    <div runat="server" id="divnews" class="col-sm-4">
                        <a href="NewsRoom.aspx">
                            <h1 style="line-height: 10px">News</h1>
                            <asp:Image ID="imgNews" CssClass="img-responsive" runat="server" Height="200px" />
                            <p runat="server" id="NewsDate" align="left">Posted </p>
                            <h5 runat="server" id="NewsTitle" align="left"></h5>
                        </a>
                    </div>
                    <div runat="server" id="divevent" class="col-sm-4">
                        <a href="Upcoming_Events.aspx">
                            <h1 style="line-height: 10px">Event</h1>
                            <asp:Image ID="imgEvent" CssClass="img-responsive" runat="server" Height="200px" />
                            <p runat="server" id="eventDate" align="left">Posted </p>
                            <h5 runat="server" id="eventTitle" align="left"></h5>
                        </a>
                    </div>
                    <div runat="server" id="divblogs" class="col-sm-4">
                        <a href="Blogs.aspx">
                            <h1 style="line-height: 10px">Blogs</h1>
                            <asp:Image ID="imgBlog" CssClass="img-responsive" runat="server" Height="200px" />
                            <p runat="server" id="blogDate" align="left">Posted </p>
                            <h5 runat="server" id="blogTitle" align="left"></h5>
                        </a>
                    </div>
                </div>
            </div>
        </section>

    </div>
</asp:Content>
