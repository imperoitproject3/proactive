﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="who_we_are.aspx.cs" Inherits="who_we_are" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Who We Are</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .WeAre {
            color: red;
            font-size: 63px;
            line-height: 70px;
            margin-bottom: 48px;
        }
    </style>
    <div class="main-container">
        <%--<header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/about.png" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h1 class="text-white">Leader in Global IP Protection</h1>
                        <p class="lead text-white">
                            Proactive channel offers a range of global services for protecting IP across borders. 
                        </p>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>--%>
        <section id="content" class="duplicatable-content" style="padding-top: 160px !important;">
            <div class="container">
                <div class="row"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="WeAre">We are</h1>
                        <h1 class="WeAre">Proactive Channel Inc.</h1>
                        <img src="img/1.jpg" width="100%" class="img-thumbnail" alt="Cinque Terre">
                        <%--<p class="lead">
                            Proactive Channel is a private limited company & trusted name in the world for online brand protection covering all the industries. We have been acquired a network of leading high-ends brands, expertise & technology to safeguard your intellectual property rights.
                        </p>--%>
                    </div>
                </div>
                
                <section id="ourpeople"> 
                    <br />
                     <br />
                    <div class="row">
                    <div class="col-md-12">
                        <h1 style="font-size: 32px;">Our People</h1>
                        <img src="img/OurPeople.jpg" width="100%" class="img-thumbnail" alt="Cinque Terre">
                    </div>
                </div>
                </section>
                 
               
                <section id="ourpurpose"> 
                    <br />
                    <br />
                  <div class="row">
                    <div class="col-md-6">                        
                        <h1>Our Purpose</h1>
                        <img src="img/3.jpg" width="100%" class="img-thumbnail" alt="Cinque Terre">
                       <%-- <p class="lead">
                            We bring down the infringement to zero level without affecting the brand integrity, reputation & revenue.
                        </p>--%>
                    </div>
                       <div class="col-md-6">
                        <h1>Our Values</h1>
                        <img src="img/2.jpg" width="100%" class="img-thumbnail" alt="Cinque Terre">
                       <%-- <p class="lead">-Relevance</p>
                        <p class="lead">-Integrity </p>
                        <p class="lead">-Excellence </p>
                        <p class="lead">Every employee is driven by the same core values: Relevance, Integrity & Excellence. </p>--%>
                    </div>
                </div>
                </section>               
                <br />
                <div class="row">
                </div>
                <section id="WeAreAection" class="duplicatable-content">
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Why Real time IP Enforcement is very vital for brands?</h1>
                            <p class="lead">Identifying the infringement as soon as it occurs & removing them in the real time is necessary for any brand or company.</p>
                            <p class="lead">If any infringing site, listings or content are missed out & not able to remove before the transactions are completed then it is worthless. Because it is already affected your brand reputation, revenue & the most important your customer who suffers. </p>
                            <p class="lead">The trend which we have seen since many years that if the monitoring is not been done in a proper manner the percentage of infringement increases & it directly impacts your brand. </p>
                            <p class="lead">We never missed out any of the infringement & find it out with in-depth research analysis with accuracy before the transaction is completed & removed them in the real-time. </p>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </div>
</asp:Content>

