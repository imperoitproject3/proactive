﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Contact Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/hero20.jpg" />
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">

                        <h1 class="text-white">Contact Us</h1>
                        <p class="lead text-white">
                            We'd love to hear from you!
                        </p>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>

        <section class="contact-thirds">
            <div class="container">

                <!--end of row-->

                <div class="row">
                    <div class="col-sm-4">
                        <h5>24/7 our highly engaged global Client Support.</h5>
                        <p>
                            From Proactive Channel, you can expect unparalleled service from a dedicated team of Brand Protection specialist who have deep industry expertise, intelligent detection across all digital platforms and effective enforcement.<br />
                            <br />
                            It is the dedication of our team that sets us apart and are available to help whether you have a simple “how to” question on any of our solution or a complex IP problems.

 <br />




                        </p>
                    </div>

                    <div class="col-sm-4 text-center">
                        <h5>UNITED STATES</h5>
                        <p class="lead">
                            C500 N Michigan Ave, Chicago, IL 60611
                        </p>
                        <h5>India</h5>
                        <p class="lead">
                            Proactive Channel Private Limited.
                            <br />
                            Ahmedabad-380001
                            <br />
                            Gujarat- India
                            <br />
                            Phone +91 9724658585
                            <a href="mailto:info@proactivechannnel.com">info@proactivechannnel.com</a>
                        </p>
                        <h5>DUBAI</h5>
                        <p class="lead">
                            Levels 42, Emirates Towers
                            <br />
                            Sheikh Zayed Road Dubai, UAE
                        </p>
                    </div>

                    <div class="col-sm-4">
                        <h5>Request Callback</h5>
                        <div class="form-wrapper clearfix">
                            <div class="form-contact email-form">
                                <div class="inputs-wrapper">

                                    <asp:TextBox ID="txtName" class="form-name validate-required" placeholder="Name" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="Submit" ID="RequiredFieldValidator1"
                                        ControlToValidate="txtName" runat="server" CssClass="redfont" ErrorMessage="Name Required"></asp:RequiredFieldValidator>

                                    <asp:TextBox placeholder="Email" ID="txtEmail" class="form-email validate-required validate-email" runat="server"></asp:TextBox>
                                    <br />
                                    <asp:RequiredFieldValidator Display="Dynamic" CssClass="redfont" ID="RequiredFieldValidator3" runat="server"
                                        ControlToValidate="txtEmail" ErrorMessage="Email Required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator Display="Dynamic" CssClass="redfont" ID="RegularExpressionValidator1" runat="server"
                                        ControlToValidate="txtEmail" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                                        ErrorMessage="Incorrect Email Address" ValidationGroup="Submit"></asp:RegularExpressionValidator>

                                    <asp:TextBox ID="txtNumber" class="form-name validate-required" placeholder="Contact Number" runat="server"></asp:TextBox>
                                    <br />
                                    <asp:RequiredFieldValidator Display="Dynamic" CssClass="redfont" ValidationGroup="Submit" ID="RequiredFieldValidator2"
                                        ErrorMessage="Please Enter Your Contact Number" runat="server" ControlToValidate="txtNumber"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtSuggestion" placeholder="Message" TextMode="MultiLine" class="form-message validate-required" runat="server"></asp:TextBox>


                                    <asp:RequiredFieldValidator Display="Dynamic" CssClass="redfont" ValidationGroup="Submit" ID="RequiredFieldValidator4"
                                        ErrorMessage="Message Required" runat="server" ControlToValidate="txtSuggestion"></asp:RequiredFieldValidator>
                                </div>
                                <asp:Button ID="btnSubmit" class="send-form" ValidationGroup="Submit" runat="server"
                                    Text="Submit" OnClick="btnSubmit_Click" />



                            </div>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
</asp:Content>

