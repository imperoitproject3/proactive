﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Social_Media_Monitoring.aspx.cs" Inherits="Cabling_Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Social Media Monitoring services by Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/socialmedia.jpg" />
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                          <h1 class="text-white">SOCIAL MEDIA MONITORING</h1>
                        <p class="lead text-white">Protect your brand’s reputation in social media and track down unauthorized use of your Intellectual Property.</p>
                       
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>

        <section class="article-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-9">
                        <div class="article-body">
                            <p class="lead">
                                How our social media monitoring program works?
							
                            </p>

                            <p>
                              Proactive Channel understand that social media is an essential part of a business strategy because they have highest traffic rating’s and reach thousands of users in a moment. These platforms enable users to easily distribute content, selling counterfeit products; other fraudulent activities are widespread and content facilitating digital piracy which damage brand’s reputation. A social media monitoring solution is necessary to detect and effective enforcement of illicit activity across all of the major social media platforms.

                            </p>




                          <p>
                              Combining our proven online brand protection expertise with our proprietary technology and best-of-breed, third-party data sources, we offer the most powerful social media monitoring and enforcement solution available today. This empowers organizations to proactively find and mitigate the threats to their brand on social media sites.

                          </p>


                            <ul class="list-group">

                                <li class="list-group-item">Comprehensive approach and effective enforcement across top social media platforms.

                                </li>
                                <li class="list-group-item">Protect unauthorized use of your valuable intellectual property.</li>
                                <li class="list-group-item">Identifying the links redirecting to the website selling counterfeit products and content facilitating digital piracy.</li>
                               
                            </ul>






                        </div>
                        <!--end of article body-->

                    </div>

                    <div class="col-sm-3 col-md-offset-1">
                        <div class="blog-sidebar">


                            <div class="sidebar-widget">
                                <h5>OUR SERVICES</h5>
                               
                                  <img src="img/Sales.jpg" /><br /> <br />Avail our vast range of services, uniquely customized for your business needs.<br /><br />
                                We have firm belief in catering to our clients strictly as per their specifications in order to facilitate their convenience in proficiently utilizing from the below services we offer.

                                <br />  <br />
                                <ul class="tags">
                                    <li><a href="Anti_Piracy.aspx">Anti-Piracy</a></li>
                                    <li><a href="Online_Market_Monitoring.aspx">Online Market Monitoring</a></li>
                                    <li><a href="Online_Brand_Protection.aspx">Online Brand Protection</a></li>
                                    <li><a href="IP_Monitoring.aspx">IP Monitoring</a></li>

                                </ul>

                              
                            </div>


                        </div>
                    </div>

                </div>
                <!--end of row-->

                <!--end of row-->

            </div>
            <!--end of container-->
        </section>

    </div>
</asp:Content>

