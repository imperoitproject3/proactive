﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Online_Brand_Protection.aspx.cs" Inherits="Cabling_Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Online Brand Protection services by Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/automation.jpg" />
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">

                        <h1 class="text-white">ONLINE BRAND PROTECTION</h1>
                        <p class="lead text-white">
                            Protect your Brand’s Reputation, Revenue & Customer Trust by Proactive Channel’s online brand protection solution.
                        </p>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>

        <section class="article-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-9">
                        <div class="article-body">
                            <p class="lead">
                               What is Online Brand Protection?
                            </p>
                           Online Brand Protection is the practice to protect Intellectual Property in order to support the business & to promote the fair & effective global ecommerce.
                            <p>
                                Online Brand Protection programme provides continuous monitoring of your products, trademark, copyrights, design & patent rights, grey markets products, trade dress, analysing the data, conducting test purchases, identifying counterfeit products & infringing content on internet platforms globally-auctions sites, ecommerce sites, B2B , B2C, C2C exchange & on social media. Deep expertise and intelligent detection across multiple platforms will help you to quantify the size and scope of illicit activity.  
                            </p>
                            <p class="lead">
                                Effective Enforcement for Online Brand Protection & Payment Blocking
                            </p>
                            <p>
                                Rapidly detects & effectively enforcement against all the infringements, reporting & removing the listings, contents, unauthorised streaming, blocking of Payments-(PAYPAL & on other methods). 
                            </p>
                            <p class="lead">
                                Repeated Infringers
                            </p>
                            <p>
                                Identifying repeated offenders and the commercial sellers selling counterfeit & pirated products in bulk quantity. Strengthening procedures for identifying and taking more effective action against repeat offenders. Increasing the use of preventive measures by intermediaries, such as filters and identity verifications and improved customer screening.<br />
                                <br />
                                Thoroughly reviewing the account of trusted sellers or power sellers selling the product at much discount price, hiding the product or circumventing through including stock images for selling fake products.
                                <br />
                                Learn why clients choose Proactive Channel for Brand Protection.
                            </p>
                            <p class="lead">
                                Conducting Test Purchases for Online Brand Protection
                            </p>
                            <p>
                                We conduct test purchases; gather evidence for any planned litigation against the companies or individual selling fake products. Our Online Brand Protection program deliver timely, prioritized, accurate, efficient and cost-effective takedown actions against all types of online brand infringements.<br />
                            </p>

                            <p class="lead">
                                ONLINE COUNTERFEITING ISSUES | IMPACTFUL BRAND PROTECTION
                            </p>
                            <p>
                                Online counterfeiting poses unique challenges to brand owners seeking to prevent this illicit trade. The Internet allows criminals to remain anonymous, thereby avoiding capture and discovery. The sale of counterfeit goods in China is increasingly shifting online, due in large part to the unique advantages that the Internet offers to counterfeiters. Ecommerce allows sellers in China to interact with and sell directly to wholesalers and consumers in the US, EU and around the world, as well as to Chinese consumers, while remaining anonymous and hiding the counterfeit nature of the goods they are selling. Furthermore, to date, there has been relatively little cross-border cooperation between authorities to stop international online counterfeiting rings. Infringers exploit your brand trust to do fraud with your customers, partners through selling counterfeit products or malware, phishing. They try to make profiting from your hard work & reputation. Counterfeiters generally sell products at price points that more closely mimic authentic pricing with only smaller promotional discounts. As a result, consumers are often unaware that they are buying a fake, but instead believe they are getting ‘a good deal’ on genuine goods.
                                <br />
                                Proactive Channel’s online brand protection solution will help you to stay ahead online with our industry expertise and continuous advancement of our technology. With right technology in place for online brand protection solution secure your brand & protect them against all the infringements.

                            </p>
                            <p class="lead">
                                Online Platforms 
                            </p>
                            <p>Online counterfeiting takes two predominant forms: sales made from stand-alone websites and sales made from online stores hosted on B2B/B2C/C2C trading platforms. In addition, social media sites and spam sales increasingly feed traffic to these websites and platforms. Each of these forms poses significant enforcement challenges:</p>
                            <p class="lead">
                                Stand-alone counterfeiting websites
                            </p>
                            <p>
                                Counterfeit stand-alone websites are generally set up to appear to consumers to be a particular brand‟s authorized website by copying the brand‟s own product photos, marketing images and text and locating the website at a cyber-squatted domain name containing the brand (e.g. cheap[brand name].com or wholesale[brand].org). These sites are written in English (or in the language of the target market) and accept credit card payments in US dollars (or in the local currency of the target market).<br />
                                Counterfeiters generally sell products at price points that more closely mimic authentic pricing with only smaller promotional discounts. As a result, consumers are often unaware that they are buying a fake, but instead believe they are getting „a good deal‟ on genuine goods.<br />
                                It is easy and inexpensive to set up a counterfeit website, therefore criminal rings simultaneously operate hundred or even thousands of websites at the same time. When one site is shut down, another immediately takes its place. As a result, certain brands are faced with thousands of counterfeit websites at any time.<br />
                                Counterfeit stand-alone websites are generally set up to appear to consumers to be a particular brand’s authorized website by copying the brand’s own product photos, marketing images and text and locating the website at a cyber-squatted domain name containing the brand (e.g. cheap[brand name].com or wholesale[brand].org). These sites are written in English (or in the language of the target market) and accept credit card payments in US dollars (or in the local currency of the target market).
                            </p>
                            <p class="lead">
                                Online Trading Platforms 
                            </p>
                            <p>
                                Counterfeiters also sell illicit goods via selling pages on online B2B/B2C/C2C trading platforms. Like standalone websites, listings on trading platforms can make a counterfeit goods seem legitimate through use of a brand‟s own product photos and descriptions and even go so far as posting fabricated „letters of authorization‟. Many platforms provide (for a fee) verification seals and badges and high search placement to make a seller seem more trustworthy than it is. Some platforms have their own payment services and escrow services that can facilitate sales and further enhance the perception that such products are legitimate. As with standalone websites, counterfeit rings operate many seemingly unrelated stores simultaneously so that if one is removed, there is no great loss. For certain platforms, all or virtually all of the branded goods being sold are counterfeit.<br />

                            </p>
                            <p class="lead">
                                Anonymity
                            </p>
                            <p>
                                Counterfeiters are able to remain anonymous in virtually every aspect of online counterfeiting. Registering a domain name, maintaining a website or selling page on a platform, accepting orders, shipping packages and processing payments can be performed using false or incomplete names making identification and capture by law enforcement or the brand extremely difficult.  A seller’s real identity is not available to consumers or brand owners.
                                <br />
                                Trading platforms and online service providers and intermediaries are lax in verifying that sellers are using accurate seller information. The information that registrants of counterfeit stand-alone websites sites provide for the WHOIS database is generally false or incomplete. Shippers and express couriers often do not require senders to supply correct or complete return addresses. Counterfeit goods are generally shipped with a fake or incomplete return address via EMS China, China Post or another courier. This anonymity makes it impossible for brands or law enforcement, especially in foreign countries, to locate the sender. Privacy laws in China further stymie the ability to discover the identities of the counterfeiters.
                            </p>
                            <p class="lead">
                                Cross-Jurisdictional Crime 
                            </p>
                            <p>
                                Online counterfeiting is a global enterprise that typically cuts across many jurisdictions. The brand owners and victims are often located outside of China, as is the evidence of the counterfeit sale and often times certain key aspects of the operation. For example, it is very common for a China-based ring to operate websites that are targeting consumers in the US and EUROPE using Internet Service Providers (ISPs) that are located in the US, with goods being transhipped through Hong Kong and Singapore, using payment processors located in the US and EUROPE, but with funds eventually flowing back to Chinese bank accounts.<br />
                                At the same time, civil lawsuits against Chinese large groups of anonymous online counterfeiters are becoming increasingly common in the US and EU, with brands obtaining injunctions to disable tens of thousands of web sites direct at residents in that jurisdiction and to freeze payment accounts found in that jurisdiction. These actions are limited, however, because it is not currently possible to enforce these actions in China where the infringers are located, as Chinese law does not recognize civil judgments issued by courts in the US and most EU member states. These types of lawsuits against anonymous online counterfeiters would not currently be possible in China, as Chinese law does not currently permit the filing of civil actions unless the identity of the defendant is clear. Given that counterfeiters are able to remain virtually anonymous in the operations, it is nearly impossible to bring civil litigation in China, especially when the counterfeiting is directed to foreign targets.
                            </p>
                            <p class="lead">
                                Worldwide global online brand protection solution-At your door step. 
                            </p>
                            <p>
                                Commitment for protecting your brand, Unrivalled insights & trends, following the best practices to identifying the infringements, providing daily enhanced reports, delivering quick results and get to the root of problem the brand is facing.<br />
                                A world class global leader in online brand protection solutions, anti-counterfeiting & anti-piracy that monitor and enforced the intellectual property rights to mitigate all type of infringements. Proactive Channel’s online brand protection program is designed specifically for our brand protection partners.<br />
                                Our Target is to bring the Intellectual Property Rights infringements to zero level. 
                            </p>
                        </div>
                        <!--end of article body-->
                    </div>
                    <div class="col-sm-3 col-md-offset-1">
                        <div class="blog-sidebar">
                            <div class="sidebar-widget">
                                <asp:Repeater ID="rptServices" runat="server">
                                    <ItemTemplate>
                                        <h2><%# Eval("StrContentType") %></h2>
                                        <img src="<%# Page.ResolveUrl("~/Files/BrandImages/"+Eval("imageName")) %>" width="250px" height="150px"><br />
                                        <a href="<%# Page.ResolveUrl("~/services.aspx?resources="+Eval("ResourceType")+"&brand="+Eval("ContentType")+"&Id="+Eval("Id")) %>">
                                            <h4><%# Eval("Title") %></h4>
                                        </a>
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end of row-->

                <!--end of row-->

            </div>
            <!--end of container-->
        </section>

    </div>
</asp:Content>

