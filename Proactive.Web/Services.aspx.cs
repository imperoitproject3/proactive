﻿using Proactive.Core.Enumerations;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Services : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["brand"]) && !string.IsNullOrEmpty(Request.QueryString["Resources"]) && !string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                var Id = Request.QueryString["Id"].ToString();
                var intId = Convert.ToInt32(Id);
                var Brand = Request.QueryString["Brand"].ToString();
                var Resources = Request.QueryString["Resources"].ToString();

                var contents = (Contents)Enum.Parse(typeof(Contents), Brand);
                var resources = (Resources)Enum.Parse(typeof(Resources), Resources);
                BindBrand(contents, resources, intId);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
    }

    private void BindBrand(Contents contents, Resources resources, int Id)
    {
        BrandServices _service = new BrandServices();
        var objService = _service.GetContentForWeb(Id);
        var objServicesList = _service.GetContentForServiceList(contents, resources, Id);
        if (objService != null)
        {
            ltlTitle.Text = objService.Title;
            ltlDescription.Text = objService.Description;
            ltrPageTitle.Text = objService.StrContentType;
            ServiceImage.ImageUrl = Page.ResolveUrl("~/Files/BrandImages/" + objService.imageName);
        }
        if (objServicesList.Count > 0)
            rptServices.DataSource = objServicesList;
        else
            rptServices.DataSource = null;
        rptServices.DataBind();
    }
}