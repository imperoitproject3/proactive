﻿using Proactive.Core.Enumerations;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLatestData();
        }


    }

    private void BindLatestData()
    {
        BlogService _service = new BlogService();

        var objBlog = _service.GetPagelatestContent(PageContent.Blogs);
        var objEvent = _service.GetPagelatestContent(PageContent.UpcomingEvents);
        var objNews = _service.GetPagelatestContent(PageContent.Newsroom);

        if (objBlog != null)
        {
            blogDate.InnerText += objBlog.Published.ToString("MMMM dd yyyy");
            blogTitle.InnerText = objBlog.Title;
            imgBlog.ImageUrl = Page.ResolveUrl("~/Files/BrandImages/" + objBlog.imageName);
        }
        else
            divblogs.Visible = false;
        if (objEvent != null)
        {
            eventDate.InnerText += objEvent.Published.ToString("MMMM dd yyyy");
            eventTitle.InnerText = objEvent.Title;
            imgEvent.ImageUrl = Page.ResolveUrl("~/Files/BrandImages/" + objEvent.imageName);
        }
        else
            divevent.Visible = false;
        if (objNews != null)
        {
            NewsDate.InnerText += objNews.Published.ToString("MMMM dd yyyy");
            NewsTitle.InnerText = objNews.Title;
            imgNews.ImageUrl = Page.ResolveUrl("~/Files/BrandImages/" + objNews.imageName);
        }
        else
            divnews.Visible = false;

    }
}