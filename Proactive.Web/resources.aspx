﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="resources.aspx.cs" Inherits="Brand_Protection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Online Brand Protection services by Proactive Channel</title>
    <style>
        .result-title {
            color: #202020;
            font-size: 1.0em;
            font-weight: 400;
            line-height: 1.45em;
            margin: 0;
            overflow: hidden;
            padding: 0 10px 10px position:relative;
            z-index: 2;
        }

        .category-title {
            color: #575757 !important;
            font-size: 1.4em;
            font-weight: 500;
            padding-top: 10px;
            line-height: 1.25em;
            margin: 0;
            overflow: hidden;
            position: relative !important;
            width: inherit;
            z-index: 2 !important;
        }

        .resourceCatLine {
            background: gray none repeat scroll 0 0;
            border: 0 none;
            height: 1px;
            margin: 0.5em auto !important;
            max-width: 65%;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/automation.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h1 class="text-white">
                            <asp:Literal ID="ltlTitle" runat="server"></asp:Literal></h1>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>
    </div>
    <section runat="server" id="secWhitePaper" class="col-md-12 text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 text-center">
                    <div class="col-md-6 text-center">Filter Result</div>
                    <div class="col-md-6">
                        <div class="Col-md-6 col-sm-12 col-xm-12 text-center">
                            <div class="form-item form-type-select form-item-field-type-tid">
                                <asp:DropDownList OnSelectedIndexChanged="rbltype_SelectedIndexChanged" ID="rbltype" AutoPostBack="true" runat="server" class="form-select" Width="100%">
                                    <asp:ListItem Value="All" Selected="True">- All -</asp:ListItem>
                                    <asp:ListItem Value="WhitePaper" Text="White Paper"></asp:ListItem>
                                    <asp:ListItem Value="Article" Text="Article"></asp:ListItem>
                                    <asp:ListItem Value="Report" Text="Report"></asp:ListItem>
                                    <asp:ListItem Value="Datasheet" Text="Data sheet"></asp:ListItem>
                                    <asp:ListItem Value="CaseStudy" Text="Case Study"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
            </div>
        </div>
        <div class="container">
            <asp:Repeater ID="rptResource" runat="server">
                <ItemTemplate>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="thought-leadership-item">
                            <div class="field-name-field-image">
                                <a href="<%# Page.ResolveUrl("~/services.aspx?resources="+Eval("ResourceType")+"&brand="+Eval("ContentType")+"&Id="+Eval("Id")) %>" class="btnResourceBlock">
                                    <img src="<%# Page.ResolveUrl("~/Files/BrandImages/"+Eval("imageName")) %>" width="100%" height="150px">
                                </a>
                            </div>
                            <a href="<%# Page.ResolveUrl("~/services.aspx?resources="+Eval("ResourceType")+"&brand="+Eval("ContentType")+"&Id="+Eval("Id")) %>" class="btnResourceBlock">
                                <div class="category-title-mask">
                                    <p class="category-title type"><%# Eval("StrContentType") %></p>
                                    <hr class="resourceCatLine">
                                    <p class="result-title"><%# Eval("Title") %></p>
                                </div>
                                <p class="result-content hidden"></p>
                            </a>
                        </div>
                        <br />
                        <br />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </section>
</asp:Content>

