﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="anticounterfeiting.aspx.cs" Inherits="anticounterfeiting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>anti counterfeiting services by Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/software.jpeg" />
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h1 class="text-white">Anti Counterfeiting</h1>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>

        <section class="article-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-9">
                        <div class="article-body">
                            <p class="lead">
                                zero tolerance policy to counterfeiting
                            </p>
                            <p>
                                The fight against counterfeiting is a long term element for any brand for
sustainability strategy. Preserving the creativity and the right of innovators, designers, artists
and brands is vital to their long-term-survival. One of the biggest threats to that survival is
counterfeiting, whose effects go far beyond buying a cheap bag on a street in a faraway city
while in holiday.<br />
                                <br />
                                Following its respect for creativity and protection of Intellectual Property Rights, Proactive
Channel has a zero tolerance policy to counterfeiting. Counterfeiting is a violation of the
talent, the skills of the craftsmen and the creativity of the artists to whom any brand owes the
success. The robbery of Intellectual Property Rights undermines the investment and
knowledge made to develop the company. Counterfeiting further damages communities with
the uncontrolled and dangerous working conditions and abuses human rights such as under-
aged and forced labour. A high price sits behind the purchase of a cheap fake product.<br />
                                <br />
                                The sale of counterfeiting goods is a serious offense. Every brand should conduct their
business with due diligence to protect consumers from the harm of fake products. Proactive
Channel believes that in both the online and offline world, opportunities and responsibilities
should be shared, based on the principle that each actor should be under a duty of care to
take all reasonable steps to protect consumers from misleading practices. We are more
determined than ever to preserve creativity in protecting its brand in the interest of its
customers, its employee, investors, owners and those who suffer at the hands of
counterfeiting industry.
                                <br />
                                <br />
                                Below are the two url on clicking on it we will redirecting to the pdf.
                                <br />
                                <br />
                                <b>Best Practices for addressing the sale of counterfeits on the internet.</b><br /><b>
Online Counterfeiting Issues and Enforcement in China</b>
                            </p>
                        </div>
                        <!--end of article body-->
                    </div>
                    <div class="col-sm-3 col-md-offset-1">
                        <div class="blog-sidebar">
                            <div class="sidebar-widget">
                                <asp:Repeater ID="rptServices" runat="server">
                                    <ItemTemplate>
                                        <h2><%# Eval("StrContentType") %></h2>
                                        <img src="<%# Page.ResolveUrl("~/Files/BrandImages/"+Eval("imageName")) %>" width="250px" height="150px"><br />
                                        <a href="<%# Page.ResolveUrl("~/services.aspx?resources="+Eval("ResourceType")+"&brand="+Eval("ContentType")+"&Id="+Eval("Id")) %>">
                                            <h4><%# Eval("Title") %></h4>
                                        </a>
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end of row-->

                <!--end of row-->

            </div>
            <!--end of container-->
        </section>

    </div>
</asp:Content>

