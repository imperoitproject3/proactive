﻿using Proactive.Core.Enumerations;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewsRoom : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        BlogService _service = new BlogService();
        var objBlogs = _service.GetBlogs(PageContent.Newsroom).ToList();
        rptBlogs.DataSource = objBlogs;
        rptBlogs.DataBind();
    }
}