﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>About Proactive Channel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder parallax-background">
                <img class="background-image" alt="Background Image" src="img/about.png" />
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">

                        <h1 class="text-white">Leader in Global IP Protection</h1>
                        <p class="lead text-white">
                            Proactive channel offers a range of global services for protecting IP across borders. 
                        </p>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>
        <section id="content" class="duplicatable-content">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>Key benefits of partnering with us</h1>
                    </div>
                </div>
                <!--end of row-->

                <div class="row">

                    <!--end 6 col-->
                    <div class="col-sm-6">
                        <div class="feature feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-shield"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Commitment</h5>
                                <p>
                                    We are committed to protect your valuable brands online. Our industry expertise, global reaches and continuous advancement of our technology platform enabling our clients to exceed their enhancement and protection needs. Robust and meaningful data and analytics, which help us to act against all the infringements across the digital platforms. 

                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="feature feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-piechart"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Enhanced Reports</h5>
                                <p>
                                    We help you to quantify the size and scope of illicit activity. We provide you with all the relevant data you need to get a detailed look at who your top infringing sellers are. Their geographies and your enforcements.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="feature feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-genius"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Unrivaled Insights and Trends</h5>
                                <p>
                                    We're constantly uncovering real time insights to give you the best possible picture of your brand in the world's online markets. Delivers key insights into how counterfeit products, piracy trends are developing.  We keep you up-to date on the critical development.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->

                    <div class="col-sm-6">
                        <div class="feature feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-scope"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Delivers quick results</h5>
                                <p>
                                    We work 24/7 to keep clients one step ahead of all the potential risk and threats that Internet offers. We manage the entire process from start to end-results are immediate.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="feature feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-linegraph"></i>
                            </div>
                            <div class="pull-right">
                                <h5>Best Practices</h5>
                                <p>
                                    Delivering the highest quality research, in-depth analysis and successful enforcement within the time which give you the accurate results. Learn more about best practices which we follow and get analysis from our experts.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="feature feature-icon-large">
                            <div class="pull-left">
                                <i class="icon icon-anchor"></i>
                            </div>
                            <div class="pull-right">
                                <h5>We get to the root of the problem.</h5>
                                <p>
                                    In today's quickly evolving, online markets, every decision is only as good as the intelligence used to make it to protect your brand. We invest in the right strategy and our dedicated team helps to identify and eliminate high-value targets. We mine through the entire digital platforms to uncover what matters.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end 6 col-->


                    <!--end of row-->
                </div>
        </section>
        <section class="no-pad clearfix">

            <div class="col-md-6 col-sm-12 no-pad">

                <div class="feature-box">

                    <div class="background-image-holder overlay">
                        <img class="background-image" alt="Background Image" src="img/header2.jpg">
                    </div>

                    <div class="inner">

                        <h1 class="text-white">WHY PROACTIVE CHANNEL?</h1>
                        <p class="text-white">
                            Powerful analysis, deep and thorough research, unrivaled insights & trends, enhanced report, industry expertise, global reach & commitment to protect your brand online.
Our mixture of proactive channel and responsive measures, consistently delivers the highest levels of successful of enforcement.
                            <br />
                            <br />
                            Effectively and efficiently manage our clients program, ensuring that objectives are fully met.
All the while, analysts work around the clock to process this information in real time.<br />
                            No downtime - results are immediate.

						
                        </p>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 no-pad">

                <div class="feature-box">

                    <div class="background-image-holder overlay">
                        <img class="background-image" alt="Background Image" src="img/hero22.jpg" />
                    </div>

                    <div class="inner">

                        <h1 class="text-white">WHY WE ARE ESSENTIAL?</h1>
                        <p class="text-white">
                            Why we're essential to brands and companies all over the world over?<br />
                            We know our client need to be able to see the clear picture of their brand across all the digital platforms as well as zero risk. Our capabilities reach every aspect of your brand in the online markets, and we're always thinking about new enforcement strategies and drive performance.
                            <br />
                            By constantly monitoring online market and engaging our client across the entire internet platforms, we create solutions today that can help clients today and tomorrow. Our intelligent solutions and benchmarks make us the ideal partner and that’s why brand, companies trust Proactive Channel.
						
                        </p>

                    </div>
                </div>
            </div>

        </section>





    </div>
</asp:Content>

