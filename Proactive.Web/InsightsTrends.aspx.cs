﻿using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InsightsTrends : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindData();
    }

    private void BindData()
    {
        InsightsTrendsService _service = new InsightsTrendsService();
        var objInsightsTrends = _service.GetInsightsTrends().ToList();

        rptInsights.DataSource = objInsightsTrends;
        rptInsights.DataBind();
    }
}