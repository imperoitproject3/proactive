﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Models
{
    public class UserAddUpdateModel
    {
        public string userId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please select Country name")]
        public int CountryId { get; set; }

        [Required]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string EmailId { get; set; }

        public Role Role { get; set; } = Role.Client;

        [Required(ErrorMessage = "Please enter {0}")]
        public string Source { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Remark { get; set; }

        public string Password { get; set; }

    }

    public class UserViewModel
    {
        public string UserId { get; set; }
        public string EmailId { get; set; }
        public string Country { get; set; }
        public string Renark { get; set; }
        public string Source { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public class UserRoleMenuModel
    {
        public int Id { get; set; }
        public Role Role { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }

    public class UserList
    {
        public string UserId { get; set; }
        public string Name { get; set; }
    }
}
