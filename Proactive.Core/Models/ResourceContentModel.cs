﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Proactive.Core.Models
{
    public class ResourceContentModel
    {
        public ResourceContentModel()
        {
            imageName = "Default.jpg";
        }

        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string imageName { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual HttpPostedFileBase fileBaseImage { get; set; }

        public Contents ContentType { get; set; }
        public string StrContentType { get; set; }

        public Resources ResourceType { get; set; }
    }
}
