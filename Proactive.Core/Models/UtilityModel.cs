﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Models
{
    public class AlertModel
    {
        public AlertModel(AlertStatus alertStatus, string alertMessage)
        {
            this.alertStatus = alertStatus;
            this.alertMessage = alertMessage;
        }
        public AlertStatus alertStatus { get; set; }
        public string alertMessage { get; set; }
    }

    public class BreadcrumbModel
    {
        public string actionTitle { get; set; }
        public string actionName { get; set; }
        public string actioncontroller { get; set; }
        public string subActionTitle { get; set; }
    }

    public class UtilityModel
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }
    public class ChartListViewModel
    {
        public ChartViewModel[] List { get; set; }
        public List<ChartViewModel> ListData { get; set; }
        public ChartViewModel[] BarchartList { get; set; }
        public DatasetModel dataset { get; set; }
        public SummaryModel Summary { get; set; }

    }
    public class DataSetValueModel
    {
        public int value { get; set; }
    }

    public class DataModel
    {
        public string seriesname { get; set; }
        public List<DataSetValueModel> data { get; set; }
    }

    public class DatasetModel
    {
        public List<DataModel> dataset { get; set; }
        public List<ChartViewModel> barchartlist { get; set; }
    }



    public class ChartViewModel
    {
        public string label { get; set; }
        public int value { get; set; }
        public string link { get; set; }
        public int open { get; set; }
        public int close { get; set; }
        public int retractbyclient { get; set; }
        public int pending { get; set; }
    }

    public class SummaryModel
    {
        public string PlateformName { get; set; }
        public int Total { get; set; }
        public int Open { get; set; }
        public int close { get; set; }
        public int pending { get; set; }
        public int retracted { get; set; }
        public int Rejected { get; set; }
        public int Approved { get; set; }
    }

    public class Product
    {
        public int ID { get; set; }
        public string Year { get; set; }
        public string Purchase { get; set; }
        public string Sale { get; set; }
    }

    public class UploadImageModel
    {
        public string imageUrl { get; set; }
        public string saveFilePath { get; set; }
        public string hdnImageName { get; set; }
    }
    public class SaveImageModel
    {
        public string imgBase64String { get; set; }
        public string saveFilePath { get; set; }
        public string extension { get; set; }
        public string previousImageName { get; set; }
    }

}
