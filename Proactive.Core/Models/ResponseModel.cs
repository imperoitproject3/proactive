﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Models
{
    public class ResponseModel<T> where T : new()
    {
        public ResponseModel()
        {
            Result = new T();
            Status = ResponseStatus.Failed;
            Message = string.Empty;
        }

        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }

    public class LookupBaseModel
    {
        public int Id { get; set; }
        [Required]
        public string name { get; set; }
    }
}
