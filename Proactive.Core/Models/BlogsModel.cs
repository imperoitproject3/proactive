﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Proactive.Core.Models
{
    public class BlogsModel
    {
        public BlogsModel()
        {
            imageName = "Default.jpg";
        }
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Description { get; set; }

        public string imageName { get; set; }

        public virtual HttpPostedFileBase fileBaseImage { get; set; }

        public PageContent PageContentType { get; set; }

        public string ContentType { get; set; }

        public DateTime Published { get; set; }
    }
}
