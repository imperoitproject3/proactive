﻿using Proactive.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Models
{
    public class InfringementAddModel
    {
        public int InfringementId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public double Price { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Url { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string ItemNumber { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string SellerLocation { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string ItemLocation { get; set; }

        public string Remark { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "Please select Client name")]
        public string ClientId { get; set; }

        [Required(ErrorMessage = "Please select Platform name")]
        public int PlatformId { get; set; }

        [Required(ErrorMessage = "Please select Infringement Type")]
        public int InfringementTypeId { get; set; }

        [Required(ErrorMessage = "Please select Seller")]
        public int SellerId { get; set; }

        public string Currency { get; set; }

        public bool IsSendForRview { get; set; } = false;

        public string imageObject { get; set; }
        public List<ImagesModel> imageList { get; set; }
    }
    public class ImagesModel
    {
        public string imageName { get; set; }
        public string imageUrl { get; set; }
    }
    public class CeaseandDesistImagesModel
    {
        public string imageName { get; set; }
        public string imageUrl { get; set; }
        public string Source { get; set; }
    }

    public class InfringementViewModel
    {
        public InfringementViewModel()
        {
            CeaseandDesistImageList = new List<CeaseandDesistImagesModel>();
            imageList = new List<ImagesModel>();
        }
        public int InfringementId { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string ItemNumber { get; set; }
        public int Quantity { get; set; }
        public string SellerLocation { get; set; }
        public string ItemLocation { get; set; }
        public string Remark { get; set; }
        public string UserId { get; set; }
        public string ClientName { get; set; }
        public string Platform { get; set; }
        public string InfringementType { get; set; }
        public bool IsSendForRview { get; set; }
        public string ImageUrl { get; set; }
        public ReviewStatus ReviewStatus { get; set; }
        public List<CeaseandDesistImagesModel> CeaseandDesistImageList { get; set; }
        public string Country { get; set; }
        public List<ImagesModel> imageList { get; set; }
        public string Reason { get; set; }
        public bool IsRepeated { get; set; }
        public DateTime ReportedTime { get; set; }
        public DateTime? RemovedTime { get; set; }
    }

    public class InfringementFilter
    {
        public string ClientId { get; set; }
        public string SearchText { get; set; }
        public int? PlatformId { get; set; }
        public int? InfringementTypeId { get; set; }
        public ReviewStatus? ReviewStatus { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class InfringementListWithFilter
    {
        public InfringementFilter InfringementFilter { get; set; }
        public IQueryable<InfringementViewModel> InfringementView { get; set; }
    }
}
