﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Models
{
    public class AddSellerModel
    {
        public int sellerId { get; set; }

        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "please enter contact detail")]
        public string Contact { get; set; }

        public bool IsLinkedAcc { get; set; }

        public bool IsHighriskAcc { get; set; }

        public List<SellerAccountsList> LinledAccList { get; set; }
        public List<SellerHighriskAccList> HighriskAccList { get; set; }
    }


    public class SellerAccountsList
    {
        public int Id { get; set; }

        public string LinkedAccName { get; set; }
    }

    public class SellerHighriskAccList
    {
        public int Id { get; set; }

        public string HighriskAccName { get; set; }
    }

    public class GetSellerListModel
    {
        public int SellerId { get; set; }

        public string Name { get; set; }

        public string Contact { get; set; }

        public List<SellerAccountsList> SellerAccounts { get; set; }

        public List<SellerHighriskAccList> SellerHighriskAccList { get; set; }
    }

    public class GetRepeatedInfregment
    {
        public int InfrigmentId { get; set; }

        public int SellerId { get; set; }

        public string Name { get; set; }

        public string ContactDetail { get; set; }

        public string PlateForm { get; set; }

        public string Country { get; set; }

        public string ClientId { get; set; }

        public int Count { get; set; }

        public List<SellerAccountsList> SellerAccounts { get; set; }

        public List<SellerHighriskAccList> SellerHighriskAccList { get; set; }
    }

    
}
