﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Models
{
    public class InsightsTrendsModel
    {
        public InsightsTrendsModel()
        {
            ClientList = new List<CheckBoxModel>();
            imageName = "Default.jpg";
        }
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter {0}")]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsViewOnWeb { get; set; }
        public List<CheckBoxModel> ClientList { get; set; }

        public string imageName { get; set; }

        public virtual string SelectedClientId { get; set; }
    }

    public class CheckBoxModel
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsChecked { get; set; }
    }
}
