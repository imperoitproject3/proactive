﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Models
{
    public class PlatformModel : LookupBaseModel
    {
        [Required]
        public string Url { get; set; }

        [Required]
        public int CountryId { get; set; }
    }
    public class PlatformViewModel : PlatformModel
    {
        public string countryName { get; set; }
    }
}
