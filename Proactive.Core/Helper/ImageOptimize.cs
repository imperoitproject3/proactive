﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Helper
{
    public static class ImageOptimize
    {
        private static Dictionary<string, ImageCodecInfo> encoders = null;

        public static Dictionary<string, ImageCodecInfo> Encoders
        {
            get
            {
                if (encoders == null)
                {
                    encoders = new Dictionary<string, ImageCodecInfo>();
                }

                if (encoders.Count == 0)
                {
                    foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                    {
                        encoders.Add(codec.MimeType.ToLower(), codec);
                    }
                }

                return encoders;
            }
        }

        public static void OptimizeSaveJpeg(string SaveFilePath, Image image, int quality)
        {
            if ((quality < 0) || (quality > 100))
            {
                string error = string.Format("Jpeg image quality must be between 0 and 100, with 100 being the highest quality.  A value of {0} was specified.", quality);
                throw new ArgumentOutOfRangeException(error);
            }

            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            image.Save(SaveFilePath, jpegCodec, encoderParams);
        }
        public static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            string lookupKey = mimeType.ToLower();

            ImageCodecInfo foundCodec = null;

            if (Encoders.ContainsKey(lookupKey))
            {
                foundCodec = Encoders[lookupKey];
            }

            return foundCodec;
        }
    }
}
