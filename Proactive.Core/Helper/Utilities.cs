﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Helper
{
    public static class Utilities
    {
        public static ResponseModel<object> SaveCropImage(string imgBase64String, string saveFilePath, string extension)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                imgBase64String = imgBase64String.Replace("data:image/png;base64,", "");

                byte[] bytes = Convert.FromBase64String(imgBase64String);

                Image image;
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }
                if (extension.ToLower() != "png")
                    ImageOptimize.OptimizeSaveJpeg(saveFilePath, image, 90);
                else
                {
                    image.Save(saveFilePath);
                }
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Image uploaded ";
            }
            catch (Exception err)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = err.Message;
            }

            return mResult;
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string memberName, bool asc = true)
        {
            ParameterExpression[] typeParams = new ParameterExpression[] { Expression.Parameter(typeof(T), "") };

            PropertyInfo pi = typeof(T).GetProperty(memberName);

            return (IOrderedQueryable<T>)query.Provider.CreateQuery(
                Expression.Call(
                    typeof(Queryable),
                    asc ? "OrderBy" : "OrderByDescending",
                    new Type[] { typeof(T), pi.PropertyType },
                    query.Expression,
                    Expression.Lambda(Expression.Property(typeParams[0], pi), typeParams))
                );
        }
    }
}
