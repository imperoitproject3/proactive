﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Helper
{
    public static class GlobalConfig
    {
        public const string ProjectName = "Proactive Channel";
        public const string DateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        public const string DisplayDateOnlyFormat = "MM/dd/yyyy";

        #region Path and URL
        public static string baseUrl = ConfigurationManager.AppSettings["BaseRouteUrl"].ToString();
        public static string baseRoutePath = ConfigurationManager.AppSettings["BaseRoutePath"];

        public static string fileRoutePath = baseRoutePath + @"Files\";
        public static string fileBaseUrl = baseUrl + "Files/";

        public static string InfringementPath = fileRoutePath + @"InfringementImage\";
        public static string InfringementUrl = fileBaseUrl + "InfringementImage/";
        
        public static string BlogImagePath = fileRoutePath + @"BlogImages\";
        public static string BlogImageUrl = fileBaseUrl + "BlogImages/";

        public static string BrandImagePath = fileRoutePath + @"BrandImages\";
        public static string BrandImageUrl = fileBaseUrl + "BrandImages/";

        public static string CeaseAndDesistPath = fileRoutePath + @"CeaseAndDesistFile\";
        public static string CeaseAndDesistUrl = fileBaseUrl + "CeaseAndDesistFile/";

        public static string EmailTemplatePath = fileRoutePath + @"EmailTemplate\";
        public static string EmailTemplateUrl = fileBaseUrl + "EmailTemplate/";

        public static string InfringementChartUrl = baseUrl + "Admin/Home/Chart";


        public const string defaultImageName = "Default.png";

        #endregion
        public const int PageSize_AdminPanel = 10;
        #region Email
        public static string EmailUserName = ConfigurationManager.AppSettings["EmailUserName"].ToString();
        public static string EmailPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
        public static string SMTPClient = ConfigurationManager.AppSettings["SMTPClient"].ToString();
        #endregion

    }
}
