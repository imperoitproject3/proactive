﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Core.Enumerations
{
    public enum Role
    {
        Admin = 1,
        Client = 2,
        User = 3
    }

    public enum ResponseStatus
    {
        Failed = 0,
        Success = 1,
        Already = 2,
        MaxAvailableSpacesFull = 3,
        Paused = 5,
        AdminBlocked = 10,
        BadRequest = 400,
        Unauthorized = 401,
        NotFound = 404,
        InternalError = 500,
        Delete = 6
    }

    public enum Platforms
    {
        Android = 1,
        iOS = 2,
        Web = 3
    }

    public enum EmailTemplate
    {
        ForgotPassword,
        RegisterClient,
        SendInvitation,
        FeedbackReply
    }

    public enum AlertStatus
    {
        Danger = 0,
        Success = 1,
        Info = 2,
        Warning = 3,
        Primary = 4
    }

    public enum ReviewStatus
    {
        Pending = 0,
        Close = 1,
        Open = 2,
        RetractByClient = 3,
        Reject = 4,
        Approved = 5
    }

    public enum Contents
    {
        [Display(Name = "White Paper")]
        WhitePaper = 1,

        [Display(Name = "Article")]
        Article = 2,

        [Display(Name = "Report")]
        Report = 3,

        [Display(Name = "Data sheet")]
        Datasheet = 4,

        [Display(Name = "Case Study")]
        CaseStudy = 5,
    }

    public enum Resources
    {
        BrandProtection = 1,
        Anticounterfeiting = 2,
        Antipiracy = 3,
        Blogs = 4,
        NewsRoom = 5,
        UpcomingEvents = 6,
        InsiteAndTrend = 7,
        PressRelease = 8
    }

    public enum PageContent
    {
        Blogs = 1,
        Newsroom = 2,
        UpcomingEvents = 3,
        InsightsAndTrends = 4,
        PressRelease = 5
    }

}