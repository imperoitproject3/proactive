﻿using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class UtilityService : IDisposable
    {
        private readonly UtilityRepository _repo;
        public UtilityService()
        {
            _repo = new UtilityRepository();
        }
        public async Task<List<ChartViewModel>> PlatformChartReports(string UserId)
        {
            return await _repo.PlatformChartReports(UserId);
        }
        public async Task<DatasetModel> PlatformBarChartReports(string UserId)
        {
            return await _repo.PlatformBarChartReports(UserId);
        }
        public async Task<List<ChartViewModel>> ChartReportByPlatform(string Name, string UserId)
        {
            return await _repo.ChartReportByPlatform(Name, UserId);
        }

        public async Task<SummaryModel> SummaryDetail()
        {
            return await _repo.SummaryDetail();
        }

        public async Task<SummaryModel> SummaryDetailByDate(DateTime date, DateTime endDate)
        {
            return await _repo.SummaryDetailByDate(date, endDate);
        }
        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
