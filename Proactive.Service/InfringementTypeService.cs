﻿using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class InfringementTypeService : IDisposable
    {
        private readonly InfringementTypeRepository  _repo;
        public InfringementTypeService()
        {
            _repo = new InfringementTypeRepository();
        }
        public InfringementTypeService(string loginUserId)
        {
            _repo = new InfringementTypeRepository(loginUserId);
        }
        public async Task<ResponseModel<object>> AddorUpdate(LookupBaseModel model)
        {
            return await _repo.AddorUpdate(model);
        }
        public async Task<ResponseModel<object>> Delete(int Id)
        {
            return await _repo.Delete(Id);
        }
        public async Task<LookupBaseModel> GetInfringementType(int Id)
        {
            return await _repo.GetInfringementType(Id);
        }
        public IQueryable<LookupBaseModel> GetInfringementType()
        {
            return _repo.GetInfringementType();
        }
        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
