﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class BrandServices
    {
        private readonly BrandRepository _repo;
        public BrandServices()
        {
            _repo = new BrandRepository();
        }

        public async Task<ResourceContentModel> GetContents(int Id)
        {
            return await _repo.GetContents(Id);
        }

        public async Task<ResponseModel<object>> AddUpdateBrandProtection(ResourceContentModel model)
        {
            return await _repo.AddUpdateBrandProtection(model);
        }

        public List<ResourceContentModel> GetContentServices(Resources Resourcetype)
        {
            return _repo.GetContentServices(Resourcetype);
        }

        public List<ResourceContentModel> GetContentList(Contents contentType, Resources Resourcetype)
        {
            return _repo.GetContentList(contentType, Resourcetype);
        }
        public List<ResourceContentModel> GetContentForServiceList(Contents contentType, Resources Resourcetype, int Id)
        {
            return _repo.GetContentForServiceList(contentType, Resourcetype, Id);
        }
        public IQueryable<ResourceContentModel> GetContentList()
        {
            return _repo.GetContentList();
        }
        public ResourceContentModel GetContentForWeb(Contents contentType, Resources Resourcetype)
        {
            return _repo.GetContentForWeb(contentType, Resourcetype);
        }
        public ResourceContentModel GetContentForWeb(int Id)
        {
            return _repo.GetContentForWeb(Id);
        }
        public List<ResourceContentModel> GetContentForWeb(Resources Resourcetype)
        {
            return _repo.GetContentForWeb(Resourcetype);
        }
        public async Task<ResponseModel<object>> Delete(int Id)
        {
            return await _repo.Delete(Id);
        }
    }
}
