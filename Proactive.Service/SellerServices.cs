﻿using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class SellerServices : IDisposable
    {
        private readonly SellerRepository _repo;
        public SellerServices()
        {
            _repo = new SellerRepository();
        }

        public List<GetSellerListModel> GetAllSellers()
        {
            return _repo.GetAllSellers();
        }

        public async Task<ResponseModel<object>> AddUpdateSeller(AddSellerModel model)
        {
            return await _repo.AddUpdateSeller(model);
        }

        public async Task<AddSellerModel> GetSellerDetail(int id)
        {
            return await _repo.GetSellerDetail(id);
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            return await _repo.Delete(Id);
        }

        public IQueryable<GetSellerListModel> GetSellers()
        {
            return _repo.GetSellers();
        }

        public List<GetRepeatedInfregment> GetRepeatedInfregment(string clientId)
        {
            return _repo.GetRepeatedInfregment(clientId);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
