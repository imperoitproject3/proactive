﻿using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class PlatformService : IDisposable
    {
        private readonly PlatformRepository _repo;
        public PlatformService()
        {
            _repo = new PlatformRepository();
        }
        public PlatformService(string loginUserId)
        {
            _repo = new PlatformRepository(loginUserId);
        }
        public async Task<ResponseModel<object>> AddorUpdate(PlatformModel model)
        {
            return await _repo.AddorUpdate(model);
        }
        public async Task<ResponseModel<object>> Delete(int Id)
        {
            return await _repo.Delete(Id);
        }
        public async Task<PlatformModel> GetPlatform(int Id)
        {
            return await _repo.GetPlatform(Id);
        }
        public IQueryable<PlatformModel> GetPlatform()
        {
            return _repo.GetPlatform();
        }
        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
