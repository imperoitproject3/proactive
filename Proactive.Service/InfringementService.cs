﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class InfringementService : IDisposable
    {
        private readonly InfringementRepository _repo;
        public InfringementService()
        {
            _repo = new InfringementRepository();
        }

        public async Task<ResponseModel<object>> AddorUpdate(InfringementAddModel model)
        {
            return await _repo.AddorUpdate(model);
        }

        public async Task<ResponseModel<object>> RemoveImage(int InfringementId, string imageName)
        {
            return await _repo.RemoveImage(InfringementId, imageName);
        }

        public IQueryable<InfringementViewModel> GetInfringement()
        {
            return _repo.GetInfringement();
        }


        //public IQueryable<InfringementViewModel> GetAllInfringementCLient(userId)
        //{

        //}

        public async Task<InfringementAddModel> GetInfringement(int Id)
        {
            return await _repo.GetInfringement(Id);
        }


        public IQueryable<InfringementViewModel> GetInfringement(string UserId)
        {
            return _repo.GetInfringement(UserId);
        }

        public IQueryable<InfringementViewModel> GetInfringementReview(string UserId)
        {
            return _repo.GetInfringementReview(UserId);
        }

        public IQueryable<InfringementViewModel> GetInfringementApprove(string UserId)
        {
            return _repo.GetInfringementApprove(UserId);
        }

        public IQueryable<InfringementViewModel> GetInfringementReject(string UserId)
        {
            return _repo.GetInfringementReject(UserId);
        }

        public IQueryable<InfringementViewModel> GetInfringementRetract(string UserId)
        {
            return _repo.GetInfringementRetract(UserId);
        }

        public IQueryable<InfringementViewModel> GetInfringementByFilter(InfringementFilter model)
        {
            return _repo.GetInfringementByFilter(model);

        }

        public IQueryable<InfringementViewModel> GetClientInfringementByFilter(InfringementFilter model, string ClientId)
        {
            return _repo.GetClientInfringementByFilter(model, ClientId);
        }

        public ResponseModel<object> CloseSelectedInfringement(string selectedInfringementIds, ReviewStatus status)
        {
            return _repo.CloseSelectedInfringement(selectedInfringementIds, status);
        }

        public ResponseModel<object> DeleteSelectedInfringement(string selectedInfringementIds)
        {
            return _repo.DeleteSelectedInfringement(selectedInfringementIds);
        }

        public async Task<ResponseModel<object>> RetractByInfregment(int Id, string Reason, ReviewStatus Status)
        {
            return await _repo.RetractByClient(Id, Reason, Status);
        }

        public async Task<ResponseModel<object>> ClosedToggle(int Id, ReviewStatus Status)
        {
            return await _repo.ClosedToggle(Id, Status);
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            return await _repo.Delete(Id);
        }

        public async Task<ResponseModel<object>> RetractDelete(int Id)
        {
            return await _repo.RetractDelete(Id);
        }

        public async Task<ResponseModel<object>> AddUpdateCeaseAndDesist(string FileName, string selectedInfringementIds, string IsClose, string Source)
        {
            return await _repo.AddUpdateCeaseAndDesist(FileName, selectedInfringementIds, IsClose, Source);
        }

        public IQueryable<InfringementViewModel> GetInfringementByStatus(ReviewStatus status, string UserId)
        {
            return _repo.GetInfringementByStatus(status, UserId);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
