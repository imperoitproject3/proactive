﻿using Microsoft.Owin;
using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class UserService : IDisposable
    {
        private UserRepository _repo;
        private readonly IOwinContext _owin;

        public UserService()
        {
            _repo = new UserRepository();
        }

        public UserService(IOwinContext owin)
        {
            _repo = new UserRepository(owin);
            this._owin = owin;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            return await _repo.AdminLogin(model);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            return await _repo.AdminChangePassword(model);
        }

        public async Task<ResponseModel<object>> Delete(string UserId)
        {
            return await _repo.Delete(UserId);
        }

        public async Task<ResponseModel<bool>> ActiveToggle(string userId)
        {
            return await _repo.ActiveToggle(userId);
        }

        public async Task<ResponseModel<object>> AddorUpdate(UserAddUpdateModel model)
        {
            return await _repo.AddorUpdate(model);
        }

        public IQueryable<UserViewModel> GetUsers()
        {
            return _repo.GetUsers();
        }

        public async Task<UserAddUpdateModel> GetUser(string userId)
        {
            return await _repo.GetUser(userId);
        }

        public async Task<ResponseModel<object>> ForgotPassword(ForgotPasswordModel model)
        {
            return await _repo.ForgotPassword(model);
        }
        public void WebLogout()
        {
            _repo.WebLogout();
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }

}
