﻿using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class InsightsTrendsService
    {
        private readonly InsightsTrendsRepository _repo;
        public InsightsTrendsService()
        {
            _repo = new InsightsTrendsRepository();
        }

        public async Task<ResponseModel<object>> AddorUpdate(InsightsTrendsModel model)
        {
            return await _repo.AddorUpdate(model);
        }

        public IQueryable<InsightsTrendsModel> GetInsightsTrends()
        {
            return _repo.GetInsightsTrends();
        }

        public IQueryable<InsightsTrendsModel> GetInsightsTrends(string UserId)
        {
            return _repo.GetInsightsTrends(UserId);
        }

        public async Task<InsightsTrendsModel> GetInsightsTrends(int Id)
        {
            return await _repo.GetInsightsTrends(Id);
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            return await _repo.Delete(Id);
        }

    }

}
