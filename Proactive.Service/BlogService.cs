﻿using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class BlogService
    {
        private readonly BlogRepository _repo;
        public BlogService()
        {
            _repo = new BlogRepository();
        }

        public async Task<ResponseModel<object>> AddorUpdate(BlogsModel model)
        {
            return await _repo.AddorUpdate(model);
        }

        public IQueryable<BlogsModel> GetBlogs(PageContent PageContent)
        {
            return _repo.GetBlogs(PageContent);
        }

        public IQueryable<BlogsModel> GetBlogs()
        {
            return _repo.GetBlogs();
        }
        public BlogsModel GetPagelatestContent(PageContent PageContent)
        {
            return _repo.GetPagelatestContent(PageContent);
        }

        public async Task<BlogsModel> GetBlogDetails(int Id)
        {
            return await _repo.GetBlogDetails(Id);
        }

        public async Task<ResponseModel<object>> Delete(int Id)
        {
            return await _repo.Delete(Id);
        }

    }
}
