﻿using Proactive.Core.Models;
using Proactive.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proactive.Service
{
    public class CountryService : IDisposable
    {
        private readonly CountryRepository _repo;
        public CountryService()
        {
            _repo = new CountryRepository();
        }
        public CountryService(string loginUserId)
        {
            _repo = new CountryRepository(loginUserId);
        }
        public async Task<ResponseModel<object>> AddorUpdate(LookupBaseModel model)
        {
            return await _repo.AddorUpdate(model);
        }
        public async Task<ResponseModel<object>> Delete(int Id)
        {
            return await _repo.Delete(Id);
        }
        public async Task<LookupBaseModel> GetCountry(int Id)
        {
            return await _repo.GetCountry(Id);
        }
        public IQueryable<CountryViewModel> GetCountries()
        {
            return _repo.GetCountries();
        }
        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
