$(document).ready(function (e) {
    document.body.style.zoom = "80%"
    BindiCheck();
});

function ActiveMenu(liId) {
    $("#li" + liId).addClass("active");
}

function deleteRow(control, Url) {
    control.children('i').removeClass('icon-trash-o').addClass('icon-spinner10 spinner');
    swal({
        title: "Are you sure to delete this record?",
        text: "You will not be able to recover this date!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DA4453",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            $.post(Url, { 'id': $(control).attr('data-id') }, function (response) {
                if (response.Status == 1) {
                    control.parent().parent().hide(200);
                    setTimeout(function () { control.remove() }, 300);
                    swal("Deleted!", response.Message, "success");
                }
                else {
                    control.children('i').removeClass('icon-spinner10 spinner').addClass('icon-trash-o');
                    swal("Error", response.Message, "error");
                }
            });
        } else {
            control.children('i').removeClass('icon-spinner10 spinner').addClass('icon-trash-o');
            swal("Cancelled", "Your record is safe :)", "info");
        }
    });
}

function StartBlockPage() {
    var block_body = $("#body");
    $(block_body).block({
        message: '<div class="icon-spinner10 spinner spinner--steps2 font-large-5"></div><br /><br /><h2>Please wait while image is being uploaded.</h2>',
        overlayCSS: {
            backgroundColor: '#FFF',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}

function CloseBlockPage() {
    var block_body = $("#body");
    $(block_body).unblock();
}

function BindRepeater() {
    $('.repeater').repeater();
}

function BindTouchspin() {
    $(".touchspin").TouchSpin({
        buttondown_class: "btn btn-danger",
        buttonup_class: "btn btn-success",
        buttondown_txt: '<i class="icon-minus4"></i>',
        buttonup_txt: '<i class="icon-plus4"></i>',
        max: 999999,
    });
}

function BindRepeater() {
    $('.repeater').repeater();
}

function BindiCheck() {
    if ($('.chk-icheck').length) {
        $('.chk-icheck').iCheck({
            checkboxClass: 'icheckbox_square-purple',
            radioClass: 'iradio_square-purple',
        });
    }
}