﻿$(document).ready(function () {
    $("#fileinputs").fileinput({
        //'theme': 'explorer',
        'uploadUrl': '#',
        showUpload: false,
        showCancel: false,
        showRemove: false,
        overwriteInitial: false,
        initialPreviewAsData: true,
        browseIcon: '<i class="icon-folder-open"></i>&nbsp;',
        browseClass: 'btn btn-info',
        fileActionSettings: {
            showUpload: false,
            showZoom: false,
            removeIcon: '<i class="icon-trash text-danger"></i>',
        },
        initialPreview: [
            //"http://lorempixel.com/1920/1080/nature/1",
            //"http://lorempixel.com/1920/1080/nature/2",
            //"http://lorempixel.com/1920/1080/nature/3"
        ],
        initialPreviewConfig: [
            //{ caption: "nature-1.jpg", size: 329892, width: "120px", url: "{$url}", key: 1 },
            //{ caption: "nature-2.jpg", size: 872378, width: "120px", url: "{$url}", key: 2 },
            //{ caption: "nature-3.jpg", size: 632762, width: "120px", url: "{$url}", key: 3 }
        ]
    });
});