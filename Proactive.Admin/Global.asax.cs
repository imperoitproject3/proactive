﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Proactive.Admin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContextWrapper context = new HttpContextWrapper(Context);
            Exception exception = Server.GetLastError();
            HttpException httpException = exception as HttpException ?? new HttpException(500, exception.Message);
            Server.ClearError();

            IController controller = null;
            RouteData routeData = null;


            controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}
