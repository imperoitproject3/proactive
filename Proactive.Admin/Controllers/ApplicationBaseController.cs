﻿using Microsoft.AspNet.Identity;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    public class ApplicationBaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (User != null)
            {
                ClaimsIdentity userIdentity = (ClaimsIdentity)User.Identity;
                if (userIdentity != null)
                {
                    IEnumerable<Claim> claims = userIdentity.Claims;
                    if (claims.Any())
                    {
                        ViewBag.FullName = claims.Where(c => c.Type == "FullName").FirstOrDefault()?.Value;
                        ViewBag.UserRole = Enum.Parse(typeof(Role), claims.Where(c => c.Type == "UserRole").FirstOrDefault()?.Value);
                    }
                }
            }
            OnInitBreadcrumb();
        }

        public void OnBindMessage(ResponseStatus status, string message)
        {
            OnBindMessage((status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger), message);
        }
        public void OnBindMessage(AlertStatus status, string message)
        {
            TempData["alertModel"] = new AlertModel(status, message);
        }

        public void OnInitBreadcrumb()
        {
            string actionName = ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = ControllerContext.RouteData.Values["controller"].ToString();

            if (controllerName != "Home")
            {
                if (actionName == "Index")
                {
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        subActionTitle = controllerName
                    };
                }
                else
                {
                    ViewBag.Action = actionName;
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        actioncontroller = controllerName,
                        actionName = "Index",
                        actionTitle = controllerName,
                        subActionTitle = actionName
                    };
                }
            }
        }

        #region Bind List
        public void BindCountry()
        {
            using (CountryService service = new CountryService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetCountries().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.name,
                                                         Value = x.Id.ToString()
                                                     }).ToList();
                ViewData["CountryList"] = dropDownList;
            }
        }

        public void BindPlatform()
        {
            using (PlatformService service = new PlatformService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetPlatform().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.name,
                                                         Value = x.Id.ToString()
                                                     }).ToList();
                ViewData["PlatformList"] = dropDownList;
            }
        }

        public void BindInfringementType()
        {
            using (InfringementTypeService service = new InfringementTypeService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetInfringementType().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.name,
                                                         Value = x.Id.ToString()
                                                     }).ToList();
                ViewData["InfringementTypeList"] = dropDownList;
            }
        }
        public void BindClient()
        {
            using (UserService service = new UserService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetUsers().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.UserId.ToString()
                                                     }).ToList();
                ViewData["ClientList"] = dropDownList;
            }
        }

        public void BindSeller()
        {
            using (SellerServices Service = new SellerServices())
            {
                List<SelectListItem> dropDownList = (from x in Service.GetSellers().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.SellerId.ToString()
                                                     }).ToList();
                ViewData["SellerList"] = dropDownList;
            }
        }

        public void BindStatus()
        {
            List<SelectListItem> StatusList = new List<SelectListItem>();
            StatusList.Add(new SelectListItem
            {
                Text = ReviewStatus.Open.ToString(),
                Value = ReviewStatus.Open.ToString(),
            });
            StatusList.Add(new SelectListItem
            {
                Text = ReviewStatus.Close.ToString(),
                Value = ReviewStatus.Close.ToString(),
            });
            StatusList.Add(new SelectListItem
            {
                Text = ReviewStatus.Pending.ToString(),
                Value = ReviewStatus.Pending.ToString(),
            });
            StatusList.Add(new SelectListItem
            {
                Text = "Retract By Client",
                Value = ReviewStatus.RetractByClient.ToString(),
            });
            ViewData["StatusList"] = StatusList;
        }

        public void ResourceType()
        {
            List<SelectListItem> ResourceTypeList = new List<SelectListItem>();
            ResourceTypeList.Add(new SelectListItem
            {
                Text = Resources.Anticounterfeiting.ToString(),
                Value = Resources.Anticounterfeiting.ToString()
            });
            ResourceTypeList.Add(new SelectListItem
            {
                Text = Resources.Antipiracy.ToString(),
                Value = Resources.Antipiracy.ToString(),
            });
            ResourceTypeList.Add(new SelectListItem
            {
                Text = Resources.BrandProtection.ToString(),
                Value = Resources.BrandProtection.ToString(),
            });
            ViewData["ResourceTypeList"] = ResourceTypeList;
        }

        public void BindCurrency()
        {
            List<SelectListItem> StatusList = new List<SelectListItem>();
            StatusList.Add(new SelectListItem
            {
                Text = "United States Dollars $",
                Value = "USD",
            });
            StatusList.Add(new SelectListItem
            {
                Text = "Euro €",
                Value = "EUR",
            });
            StatusList.Add(new SelectListItem
            {
                Value = "GBP",
                Text = "United Kingdom Pounds £",
            });
            StatusList.Add(new SelectListItem
            {
                Value = "INR",
                Text = "India Rupees ₨",
            });
            StatusList.Add(new SelectListItem
            {
                Value = "IDR",
                Text = "Indonesia Rupiahs Rp",
            });
            StatusList.Add(new SelectListItem
            {
                Value = "SGD",
                Text = "Singapore Dollars $",
            });
            StatusList.Add(new SelectListItem
            {
                Value = "THB",
                Text = "Thailand Baht ฿",
            });
            StatusList.Add(new SelectListItem
            {
                Value = "AUD",
                Text = "Australia Dollars $",
            });
            ViewData["CurrencyList"] = StatusList.OrderBy(x => x.Text);
        }
        #endregion
    }
}