﻿using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Data.Repository;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin, Role.Client)]
    public class ResourceController : ApplicationBaseController
    {
        private readonly BrandServices _service;

        public ResourceController()
        {
            ResourceType();
            _service = new BrandServices();
        }

        public ActionResult Index()
        {
            return View(_service.GetContentList());
        }

        public ActionResult Add()
        {
            ViewBag.Action = "Add";
            ResourceContentModel model = new ResourceContentModel();
            return View("AddorUpdate", model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            BindCountry();
            ViewBag.Action = "Add";
            var model = await _service.GetContents(Id);
            if (model != null)
            {
                return View("AddorUpdate", model);
            }
            OnBindMessage(AlertStatus.Danger, "Resource can't be found");
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(ResourceContentModel model)
        {
            BindClient();
            if (ModelState.IsValid)
            {
                if (model.fileBaseImage != null)
                {
                    string ogFileName = model.fileBaseImage.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                    model.fileBaseImage.SaveAs(Path.Combine(GlobalConfig.BrandImagePath, thisFileName));
                    model.imageName = thisFileName;
                }
                ResponseModel<object> mResult = await _service.AddUpdateBrandProtection(model);
                OnBindMessage(mResult.Status, mResult.Message);
            }
            return RedirectToAction("index");
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}