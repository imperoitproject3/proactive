﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        public ActionResult Index(int errorCode, string errorTitle, string errorDescription)
        {
            ViewBag.errorCode = errorCode;
            ViewBag.errorTitle = errorTitle;
            ViewBag.errorDescription = errorDescription;
            return View();
        }
        public ActionResult NotFound()
        {
            return View();
        }
        public ActionResult BadRequest(string errorTitle, string errorDescription)
        {
            ViewBag.errorTitle = errorTitle;
            ViewBag.errorDescription = errorDescription;
            return View();
        }
        public ActionResult ServerError(string errorTitle, string errorDescription)
        {
            ViewBag.errorTitle = errorTitle;
            ViewBag.errorDescription = errorDescription;
            return View();
        }
        public ActionResult Unauthorized()
        {
            return View();
        }
        public ActionResult InvalidAccess()
        {
            return View();
        }
    }
}