﻿using Microsoft.AspNet.Identity;
using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin, Role.Client)]
    public class SellerController : ApplicationBaseController
    {
        // GET: Seller
        private readonly SellerServices _service;
        public SellerController()
        {
            _service = new SellerServices();
        }

        public ActionResult Index()
        {
            ViewBag.action = "Add";
            List<GetSellerListModel> model = _service.GetAllSellers();
            return View(model);
        }

        public ActionResult RepeatedInfregment()
        {
            string userid = User.Identity.GetUserId();
            List<GetRepeatedInfregment> model = _service.GetRepeatedInfregment(userid);
            return View(model);
        }

        //[HttpPost]
        //public Task<JsonResult> RepeatedInfregments(DataTableAjaxPostModel model)
        //{
        //    DataTableAjaxResultModel result = await _service.GetFilteringList(model);
        //    return Json(result);
        //}

        public ActionResult Add()
        {
            ViewBag.action = "Add";
            return View("AddOrUpdate");
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddSellerModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if(ModelState.IsValid)
            {
                mResult = await _service.AddUpdateSeller(model);

            }
            if(mResult.Status==ResponseStatus.Success)
            {
                OnBindMessage(AlertStatus.Success, mResult.Message);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public async Task<ActionResult> Edit(int id)
        {
            ViewBag.action = "Edit";
            AddSellerModel model = await _service.GetSellerDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddorUpdate", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(AddSellerModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {

                mResult = await _service.AddUpdateSeller(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }

        public async Task<JsonResult> Delete(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}