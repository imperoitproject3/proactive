﻿using Microsoft.AspNet.Identity;
using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin, Role.Client)]
    public class HomeController : ApplicationBaseController
    {
        private readonly UtilityService _service;
        public HomeController()
        {
            _service = new UtilityService();
        }

        public async Task<ActionResult> Index()
        {
            var UserId = User.Identity.GetUserId();
            List<ChartViewModel> list = await _service.PlatformChartReports(UserId);
            DatasetModel Datasetlist = await _service.PlatformBarChartReports(UserId);
            list.ForEach(x => x.link = Url.Action("Chart", "Home", new { @Name = x.label }));
            ChartListViewModel model = new ChartListViewModel();
            model.Summary = await _service.SummaryDetail();
            model.List = list.ToArray();
            model.BarchartList = Datasetlist.barchartlist.ToArray();
            if (list.Count == 1)
                return RedirectToAction("Chart", "Home", new { Name = list.FirstOrDefault().label });
            model.dataset = Datasetlist;
            model.ListData = list;
            model.BarchartList = model.BarchartList;
            return View(model);
        }

        public async Task<ActionResult> ViewByClientId(string UserId)
        {
            //var UserId = User.Identity.GetUserId();
            List<ChartViewModel> list = await _service.PlatformChartReports(UserId);
            DatasetModel Datasetlist = await _service.PlatformBarChartReports(UserId);
            list.ForEach(x => x.link = Url.Action("Chart", "Home", new { @Name = x.link }));
            ChartListViewModel model = new ChartListViewModel();
            model.List = list.ToArray();
            if (list.Count == 1)
                return RedirectToAction("Chart", "Home", new { Name = list.FirstOrDefault().label });
            model.dataset = Datasetlist;
            model.ListData = list;
            return View("Index", model);
        }

        public async Task<ActionResult> Chart(string Name)
        {
            var UserId = User.Identity.GetUserId();
            List<ChartViewModel> list = await _service.ChartReportByPlatform(Name, UserId);
            ChartListViewModel model = new ChartListViewModel();
            model.List = list.ToArray();
            model.ListData = list;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SummaryByDate(DateTime StartDate, DateTime EndDate)
        {
            SummaryModel Summary = await _service.SummaryDetailByDate(StartDate, EndDate);
            return PartialView("_Summary", Summary);
        }
    }
}