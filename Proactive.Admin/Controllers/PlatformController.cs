﻿using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin)]
    public class PlatformController : ApplicationBaseController
    {
        private readonly PlatformService _service;
        public PlatformController()
        {
            _service = new PlatformService();
        }
        public ActionResult Index()
        {
            return View(_service.GetPlatform());
        }
        [HttpGet]
        public ActionResult Add()
        {
            BindCountry();
            return View("AddorUpdate");
        }
        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            BindCountry();
            PlatformModel model = await _service.GetPlatform(Id);
            if (model != null)
            {
                return View("AddorUpdate", model);
            }
            OnBindMessage(AlertStatus.Danger, "Resource can't be found");
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<ActionResult> Add(PlatformModel model)
        {
            BindCountry();
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("AddorUpdate", model);
        }
        [HttpPost]
        public async Task<ActionResult> Edit(PlatformModel model)
        {
            BindCountry();
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("AddorUpdate", model);
        }
        [HttpPost]
        public async Task<JsonResult> Delete(int Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}