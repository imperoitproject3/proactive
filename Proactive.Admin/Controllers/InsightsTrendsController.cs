﻿using Microsoft.AspNet.Identity;
using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin, Role.Client)]
    public class InsightsTrendsController : ApplicationBaseController
    {
        private readonly InsightsTrendsService _service;
        public InsightsTrendsController()
        {
            _service = new InsightsTrendsService();

        }

        public ActionResult Index()
        {
            return View(_service.GetInsightsTrends());
        }
        public ActionResult IndexByUser()
        {
            var UserId = User.Identity.GetUserId();
            return View(_service.GetInsightsTrends(UserId));
        }

        public ActionResult Add()
        {
            InsightsTrendsModel model = new InsightsTrendsModel();
            using (UserService service = new UserService())
            {
                foreach (var item in service.GetUsers().ToList())
                {
                    model.ClientList.Add(new CheckBoxModel() { Value = item.UserId, Text = item.Name });
                }
            }
            return View("AddorUpdate", model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            BindClient();
            InsightsTrendsModel model = await _service.GetInsightsTrends(Id);
            if (model != null)
            {
                return View("AddorUpdate", model);
            }
            OnBindMessage(AlertStatus.Danger, "Resource can't be found");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Edit(InsightsTrendsModel model)
        {
            BindClient();
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("AddorUpdate", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(InsightsTrendsModel model)
        {
            BindClient();
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("AddorUpdate", model);
        }
        [HttpPost]
        public async Task<JsonResult> Delete(int Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

    }
}