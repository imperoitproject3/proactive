﻿using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin)]
    public class CountryController : ApplicationBaseController
    {
        private readonly CountryService _service;
        public CountryController()
        {
            _service = new CountryService();
        }
        public ActionResult Index()
        {
            return View(_service.GetCountries());
        }
        [HttpGet]
        public ActionResult Add()
        {

            return View("AddorUpdate");
        }
        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            LookupBaseModel model = await _service.GetCountry(Id);
            if (model != null)
            {
                return View("AddorUpdate", model);
            }
            OnBindMessage(AlertStatus.Danger, "Resource can't be found");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Add(LookupBaseModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(LookupBaseModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}