﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin, Role.Client)]
    public class InfringementController : ApplicationBaseController
    {
        private readonly InfringementService _service;

        public InfringementController()
        {
            _service = new InfringementService();
        }

        public ActionResult Index()
        {
            BindList();
            InfringementListWithFilter model = new InfringementListWithFilter();
            IQueryable<InfringementViewModel> obj = _service.GetInfringement();
            model.InfringementView = obj;
            return View(model);
        }

        public void ExportInfringementData()
        {
            IQueryable<InfringementViewModel> obj = _service.GetInfringement();
            WebGrid grid = new WebGrid(source: obj, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
            columns: grid.Columns(
                grid.Column("Platform", "Platform"),
                grid.Column("Country", "Country"),
                grid.Column("Title", "Title"),
                grid.Column("Url", "Url"),
                grid.Column("Price", "Price"),
                grid.Column("Quantity", "Quantity"),
                grid.Column("ItemNumber", "ItemNumber"),
                grid.Column("SellerLocation", "SellerLocation"),
                grid.Column("InfringementType", "InfringementType"),
                grid.Column("Remark", "Remark"),
                grid.Column("ItemLocation", "ItemLocation"),
                grid.Column("ClientName", "ClientName"),
                grid.Column("ReviewStatus", "ReviewStatus")
                )).ToString();
            Response.ClearContent();

            Response.AddHeader("content-disposition", "attachment; filename=Infringement.xls");

            Response.ContentType = "application/excel";

            Response.Write(gridData);

            Response.End();
        }

        public ActionResult AllInfringement()
        {
            BindList();
            InfringementListWithFilter model = new InfringementListWithFilter();
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringement(UserId);
            model.InfringementView = objData;
            return View("AllInfringements", model);
        }

        public void ExportClientInfringementData()
        {
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> obj = _service.GetInfringement(UserId);
            WebGrid grid = new WebGrid(source: obj, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
            columns: grid.Columns(
                grid.Column("InfringementId", "InfringementId"),
                grid.Column("Platform", "Platform"),
                grid.Column("Country", "Country"),
                grid.Column("Title", "Title"),
                grid.Column("Url", "Url"),
                grid.Column("Price", "Price"),
                grid.Column("Quantity", "Quantity"),
                grid.Column("ItemNumber", "ItemNumber"),
                grid.Column("SellerLocation", "SellerLocation"),
                grid.Column("InfringementType", "InfringementType"),
                grid.Column("Remark", "Remark"),
                grid.Column("ItemLocation", "ItemLocation"),
                grid.Column("ClientName", "ClientName"),
                grid.Column("ReviewStatus", "ReviewStatus"),
                grid.Column("IsSendForRview", "IsSendForRview")
                )).ToString();
            Response.ClearContent();

            Response.AddHeader("content-disposition", "attachment; filename=ClientInfringement.xls");

            Response.ContentType = "application/excel";

            Response.Write(gridData);

            Response.End();
        }



        public ActionResult AllInfringementReview()
        {
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringementReview(UserId);
            return View(objData);
        }

        public ActionResult Accepted()
        {
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringementApprove(UserId);
            return View("InfringementAccepted", objData);
        }

        public ActionResult Retract()
        {
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringementRetract(UserId);
            return View("InfringementRetract", objData);
        }

        public ActionResult Rejected()
        {
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringementReject(UserId);
            return View("InfringementRejected", objData);
        }

        public ActionResult Closed()
        {
            ViewBag.action = "Closed";
            InfringementListWithFilter model = new InfringementListWithFilter();
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringementByStatus(ReviewStatus.Close, UserId);
            return View("InfringmentClosed", objData);
        }

        public ActionResult Opened()
        {
            ViewBag.action = "Reported";
            var UserId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringementByStatus(ReviewStatus.Open, UserId);
            return View("InfringmentClosed", objData);
        }

        [HttpPost]
        public PartialViewResult InfringementByFilter(InfringementListWithFilter model)
        {
            IQueryable<InfringementViewModel> objData = _service.GetInfringementByFilter(model.InfringementFilter);
            return PartialView("_infringementFilter", objData);
        }

        [HttpPost]
        public PartialViewResult ClientInfringementByFilter(InfringementListWithFilter model)
        {
            string clientid = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetClientInfringementByFilter(model.InfringementFilter, clientid);
            return PartialView("_infringementFilterClient", objData);
        }

        [HttpPost]
        public PartialViewResult InfringementViewall()
        {
            IQueryable<InfringementViewModel> objData = _service.GetInfringement();
            return PartialView("_infringementFilter", objData);
        }

        [HttpPost]
        public PartialViewResult ClientInfringementViewall()
        {
            string userId = User.Identity.GetUserId();
            IQueryable<InfringementViewModel> objData = _service.GetInfringement(userId);
            return PartialView("_infringementFilter", objData);
        }

        public async Task<ActionResult> Add()
        {
            BindList();
            BindSeller();
            InfringementAddModel model = new InfringementAddModel();
            return View("AddorUpdate", model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            InfringementAddModel model = await _service.GetInfringement(Id);
            if (model != null)
            {
                BindList();
                BindSeller();
                return View("AddorUpdate", model);
            }

            OnBindMessage(AlertStatus.Danger, "Resource can't be found");
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<ActionResult> Add(InfringementAddModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserId = User.Identity.GetUserId();
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                OnBindMessage(AlertStatus.Danger, "please enter all required filed!");
                BindList();
            }
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> AddCeaseAndDesist(string CeaseselectedInfringementIds, string chkCloseInfringement, string txtSource)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                //get filename
                string ogFileName = file.FileName.Trim('\"');
                string shortGuid = ShortGuid.NewGuid().ToString();
                var thisFileName = shortGuid + Path.GetExtension(ogFileName);

                file.SaveAs(Path.Combine(GlobalConfig.CeaseAndDesistPath, thisFileName));
                mResult = await _service.AddUpdateCeaseAndDesist(thisFileName, CeaseselectedInfringementIds, chkCloseInfringement, txtSource);
                OnBindMessage(mResult.Status, mResult.Message);
            }
            else
            {
                OnBindMessage(AlertStatus.Danger, "Please upload document!");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Edit(InfringementAddModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserId = User.Identity.GetUserId();
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                BindList();
            }
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public JsonResult UploadImages()
        {
            List<string> fileNameList = new List<string>();
            var files = Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                var file = files[i];
                string ext = Path.GetExtension(file.FileName);
                string fileName = ShortGuid.NewGuid() + ext;
                string filePath = GlobalConfig.InfringementPath + fileName;
                file.SaveAs(filePath);
                fileNameList.Add(fileName);
            }
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = files.Count + " files uploaded",
                Result = fileNameList[0]
            };
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> RemoveImage(int InfringementId, string imageName)
        {
            if (!string.IsNullOrEmpty(imageName))
            {
                string filePath = GlobalConfig.InfringementPath + imageName;
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            ResponseModel<object> mResult = await _service.RemoveImage(InfringementId, imageName);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> RetractDelete(int Id)
        {
            ResponseModel<object> mResult = await _service.RetractDelete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        private void BindList()
        {
            BindCountry();
            BindClient();
            BindPlatform();
            BindInfringementType();
            BindStatus();
            BindCurrency();
        }

        public ActionResult CloseSelectedInfringement(string selectedInfringementIds)
        {
            ResponseModel<object> mResult = _service.CloseSelectedInfringement(selectedInfringementIds, ReviewStatus.Close);
            OnBindMessage(mResult.Status, mResult.Message);
            return RedirectToAction("Index");
        }

        public ActionResult DeleteSelectedInfringement(string selectedInfringementIds)
        {
            ResponseModel<object> mResult = _service.DeleteSelectedInfringement(selectedInfringementIds);
            OnBindMessage(mResult.Status, mResult.Message);
            return RedirectToAction("Index");
        }



        [HttpPost]
        public async Task<ActionResult> RetractByClient(int InfId, string reasonText)
        {
            ResponseModel<object> mResult = await _service.RetractByInfregment(InfId, reasonText, ReviewStatus.RetractByClient);
            return RedirectToAction("Accepted");
        }

        [HttpPost]
        public async Task<JsonResult> CloseInfringement(int Id)
        {
            ResponseModel<object> mResult = await _service.ClosedToggle(Id, ReviewStatus.Reject);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> OpenInfringement(int Id)
        {
            ResponseModel<object> mResult = await _service.ClosedToggle(Id, ReviewStatus.Approved);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> ConvertCurrency(float Price, string FromCurrency)
        {
            var testcase = FromCurrency + "_USD";
            WebClient web = new WebClient();
            const string ConverterApiURL = "https://free.currencyconverterapi.com/api/v6/convert?q={0}_{1}&compact=ultra&apiKey=718ed6b8146bf3f2894f";
            
            string url = String.Format(ConverterApiURL, FromCurrency, "USD");

            string response = new WebClient().DownloadString(url);

            double test = double.Parse(response.Replace("{", "").Replace("}", "").Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries).Skip(1).FirstOrDefault().Trim());

            var data = (JObject)JsonConvert.DeserializeObject(response);
            //var result = data.SelectToken(testcase + ".val").ToString();
            //var basePrice = float.Parse(result);

            double exchangeRate = Price * test;
            var responce = Math.Round(exchangeRate, 2);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }

        //datatable 

        //public ActionResult Infrigments()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public async Task<JsonResult> Infrigments(DataTableAjaxPostModel model)
        //{
        //    DataTableAjaxResultModel result = await _service.GetFilteringList(model);
        //    return Json(result);
        //}
    }
}