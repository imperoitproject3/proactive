﻿using Microsoft.AspNet.Identity;
using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    public class AccountController : Controller
    {
        private UserService _service;

        [AllowAnonymous]
        public ActionResult Login()
        {
            AdminLoginModel model = new AdminLoginModel();
            if (User.Identity.IsAuthenticated)
                return RedirectToActionPermanent("Index", "Home");
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(AdminLoginModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());
                ResponseModel<object> mResult = await _service.AdminLogin(model);
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToAction("Index", "Home");
                ModelState.AddModelError("", mResult.Message);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            if (User != null)
            {
                ClaimsIdentity userIdentity = (ClaimsIdentity)User.Identity;
                if (userIdentity != null)
                {
                    IEnumerable<Claim> claims = userIdentity.Claims;
                    if (claims.Any())
                    {
                        ViewBag.FullName = claims.Where(c => c.Type == "FullName").FirstOrDefault()?.Value;
                        ViewBag.UserRole = Enum.Parse(typeof(Role), claims.Where(c => c.Type == "UserRole").FirstOrDefault()?.Value);
                    }
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(AdminChangePasswordModel model)
        {
            _service = new UserService(Request.GetOwinContext());
            ResponseModel<object> mResult = await _service.AdminChangePassword(model);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger, mResult.Message);
            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> ForgotPassword(ForgotPasswordModel model)
        {
            _service = new UserService(Request.GetOwinContext());
            ResponseModel<object> mResult = await _service.ForgotPassword(model);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            _service = new UserService(Request.GetOwinContext());
            _service.WebLogout();
            return RedirectToActionPermanent("Login", "Account");
        }
    }
}