﻿using Microsoft.AspNet.Identity;
using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Helper;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin, Role.Client)]
    public class BlogController : ApplicationBaseController
    {
        private readonly BlogService _service;
        public BlogController()
        {
            ResourceType();
            _service = new BlogService();

        }

        public ActionResult Index(PageContent pagecontent)
        {
            ViewBag.active = pagecontent.ToString();
            if (pagecontent == PageContent.Blogs)
            {
                ViewBag.TitleName = "Blogs";
            }
            else if (pagecontent == PageContent.Newsroom)
            {
                ViewBag.TitleName = "NewsRoom";
            }
            else if (pagecontent == PageContent.PressRelease)
            {
                ViewBag.TitleName = "Press Release";
            }
            else if (pagecontent == PageContent.UpcomingEvents)
            {
                ViewBag.TitleName = "Upcoming Events";
            }
            else
            {
                ViewBag.TitleName = "Insights & trends";
            }
            return View(_service.GetBlogs(pagecontent));
        }
        public ActionResult Add()
        {
            BlogsModel model = new BlogsModel();
            return View("AddorUpdate", model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            BlogsModel model = await _service.GetBlogDetails(Id);
            if (model != null)
            {
                return View("AddorUpdate", model);
            }
            OnBindMessage(AlertStatus.Danger, "Resource can't be found");
            return RedirectToAction("Index", new { @pagecontent = model.PageContentType });
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(BlogsModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index", new { @pagecontent = model.PageContentType });
                }
            }
            return View("AddorUpdate", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(BlogsModel model)
        {
            BindClient();
            if (ModelState.IsValid)
            {
                if (model.fileBaseImage != null)
                {
                    string ogFileName = model.fileBaseImage.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                    model.fileBaseImage.SaveAs(Path.Combine(GlobalConfig.BlogImagePath, thisFileName));
                    model.imageName = thisFileName;
                }
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index", new { @pagecontent = model.PageContentType });
                }
            }
            return View("AddorUpdate", model);
        }
        [HttpPost]
        public async Task<JsonResult> Delete(int Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}