﻿using Proactive.Admin.Filters;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proactive.Admin.Controllers
{
    [CustomAuthorization(Role.Admin)]
    public class UserController : ApplicationBaseController
    {
        private UserService _service;
        public UserController()
        {
            _service = new UserService();
        }

        public ActionResult Index()
        {
            BindCountry();
            return View(_service.GetUsers());
        }

        public async Task<ActionResult> Add()
        {
            BindCountry();
            UserAddUpdateModel model = new UserAddUpdateModel();
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(UserAddUpdateModel model)
        {
            BindCountry();

            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(string userId)
        {
            BindCountry();
            UserAddUpdateModel model = await _service.GetUser(userId);
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(UserAddUpdateModel model)
        {
            BindCountry();
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AddorUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("AddorUpdate", model);
        }
        [HttpPost]
        public async Task<JsonResult> ActiveToggle(string Id)
        {
            ResponseModel<bool> mResult = await _service.ActiveToggle(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> Delete(string Id)
        {
            ResponseModel<object> result = await _service.Delete(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}