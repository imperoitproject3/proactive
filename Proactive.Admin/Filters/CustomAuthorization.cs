﻿using Microsoft.AspNet.Identity;
using Proactive.Core.Enumerations;
using Proactive.Core.Models;
using Proactive.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Proactive.Admin.Filters
{
    public static class UserRoleMenu
    {
        public static List<UserRoleMenuModel> GetUserRoleMenu(string UserId, Role UserRole)
        {
            List<UserRoleMenuModel> objUserRoles = new List<UserRoleMenuModel>
            {
                new UserRoleMenuModel{Role=UserRole,Controller="Home",Action="Index" }
            };
            return objUserRoles;
        }
    }

    public class CustomAuthorization : AuthorizeAttribute
    {
        private bool isLoggedIn = false;

        private Role loginRole;
        private Role[] userRoles;
        private bool isAuthenticated = false;
        private bool isRoleExists = false;
        public CustomAuthorization(params Role[] roles) : base()
        {
            userRoles = roles;
        }


        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            isAuthenticated = false;
            isRoleExists = false;
            if (httpContext.User.Identity.IsAuthenticated)
            {
                ClaimsIdentity userIdentity = (ClaimsIdentity)httpContext.User.Identity;
                if (userIdentity != null)
                {
                    IEnumerable<Claim> claims = userIdentity.Claims;
                    if (claims.Any())
                    {
                        loginRole = (Role)Enum.Parse(typeof(Role), claims.Where(c => c.Type == "UserRole").FirstOrDefault()?.Value);
                    }
                }
                isAuthenticated = true;
                base.AuthorizeCore(httpContext);
                isRoleExists = userRoles.Contains(loginRole);
            }
            return isRoleExists;

            //base.AuthorizeCore(httpContext);
            //ClaimsIdentity Identity = (ClaimsIdentity)httpContext.User.Identity;
            //if (!Identity.IsAuthenticated)
            //    return false;

            //isLoggedIn = true;

            //string UserId = httpContext.User.Identity.GetUserId();

            //string claimRole = ((ClaimsIdentity)httpContext.User.Identity).Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            //Role UserRole = (Role)Enum.Parse(typeof(Role), claimRole, true);



            //string Controller = httpContext.Request.RequestContext.RouteData.GetRequiredString("controller");
            //string Action = httpContext.Request.RequestContext.RouteData.GetRequiredString("action");

            //List<UserRoleMenuModel> UserRoleMenus = UserRoleMenu.GetUserRoleMenu(UserId, UserRole);
            //if (UserRole == Role.Admin || (UserRole == Role.Client && UserRoleMenus.Any(x => x.Controller.IEquals(Controller) && x.Action.IEquals(Action))) || (UserRole == Role.User && UserRoleMenus.Any(x => x.Controller.IEquals(Controller) && x.Action.IEquals(Action))))
            //    return true;
            //return false;
        }

        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if (isLoggedIn)
            {
                filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new
                                {
                                    controller = "Home",
                                    action = "Index"
                                }));
            }
            else
            {
                filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new
                                {
                                    controller = "Error",
                                    action = "Unauthorized"
                                }));
            }
        }
    }
}